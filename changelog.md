# Gravity Change Log

## 7 May 2019 v2.1

[UPDATE] implemented improved security when updating users
[FIX] bug not showing total active accounts on config dashboard

## 14 Apr 2019 v2.0

[NEW] homepage, pricing and auth page designs
[NEW] API uses token authentication
[NEW] router middleware authentication
[NEW] viewController to reduce complexity of router.js

[UPDATE] implemented proper API routing and methods
[UPDATE] form urls and methods to utilise new API
[UPDATE] improved permission handling
[UPDATE] improved structure of controller and models
[UPDATE] moved stripe methods into separate stripe model
[UPDATE] improved view model to properly utilise respective models
[UPDATE] AJAX request now use proper error codes
[UPDATE] centralised error handling into use() – no more try/catch
[UPDATE] can no longer edit config at runtime

[FIX] account.create saving stripe id on error
[FIX] bug with config/account not updating email

## 27 Feb 2019

[NEW] Added admin for accounts and users located at /config/accounts and /config/users

## 5 Feb 2019

[UPDATE] added verifySubscription to account.js – automatically checks for
an active stripe subscription on signin

## 16 Jan 2019

[FIX] fixed validation issue with emails using 4 letter domains extensions
[FIX] fixed issue with gulp watch only running once
[UPDATE] model & controllers updated to use async/await
[UPDATE] removed has_users table: tables are now linked using foreign key

## 6 Dec 2018 v1.1

[NEW] forms can be automatically validated & submitted with submitAJAXForm()
[NEW] setup process is now managed in the browser at /config/setup
[NEW] Internal config dashboard added at /config (run config/setup first)
[NEW] Introduced metrics available at /config
[NEW] isURLValid() added to form.js to check input field for a valid URL string
[NEW] isPhoneValid() added to form.js to check input field for a valid phone number
[NEW] isSelect() valid added to form.js to check that an option has been selected

[UPDATE] Docs now located at https://usegravity.app/docs
[UPDATE] Stripe publishable API key is automatically written to file during setup
[UPDATE] settings.js changed to settings.json – update any references to the settings file
[UPDATE] chart functions now show a blank message when there is no data to show
[UPDATE] isRadioValid(), isCheckboxValid(), isSelectValid() now re-validate when an option is selected
[UPDATE] database should be initialised in each model with db.init()

[FIX] Resolved broken link to /home/privacy in router.js
[FIX] Plan field length in database updated to 32 chars to accommodate long Stripe plan IDs
[FIX] Prefixes added to animations
