const settings = require("./settings");
const express = require('express');
const auth = require("./model/auth");
const masterdata = require("./model/masterdata");
const user = require("./model/user");
const viewController = require("./controller/viewController");
const userController = require("./controller/userController");
const masterController = require("./controller/masterController");
const config = require("./model/config");
const router = express.Router();

/*
* caller function for global error handling
* route all calls through this to try and handle errors
*/

const use = fn => (req, res, next) =>
  Promise.resolve(fn(req, res, next)).catch(next);

// website routes
router.get('/', use(viewController.home));

router.get("/pricing", use(viewController.pricing));

router.get("/terms", use(viewController.terms));

router.get("/privacy", use(viewController.privacy));

router.get("/refund", use(viewController.refund));

// auth routes
router.get("/signup", use(viewController.signup));

router.get("/signup/user", use(viewController.signup.user));

router.get("/signin", use(viewController.signin));

router.get("/signout", use(viewController.signout));

router.get("/forgotpassword", use(viewController.forgotpassword));

// Buy Plan Routes
router.get('/buyplan', use(viewController.buyplan));


// app routes
router.get("/dashboard", auth.access("user", true), use(viewController.dashboard));

router.get("/account/profile", auth.access("user", true), use(viewController.account));

router.get("/account/password", auth.access("user", true), use(viewController.account.password));

router.get("/account/billing", auth.access("user", true), use(viewController.account.billing));

router.get("/account/billing/failed", use(viewController.account.billing.failed));

router.get("/account/users", auth.access("admin"), use(viewController.account.users));

router.get("/user/resetpassword", use(viewController.resetpassword));

router.get("/state", use(masterController.state));



router.get("/:state/county", use(masterController.county));

router.get("/:state/:county/city", use(masterController.city));

router.get("/:state/:county/:city/hospitals", use(masterController.hospitals));

router.get("/:state/:county/:city/:type/:hospitals/Financial_Year", use(masterController.Financial_Year));

router.get("/:state/:county/:city/:type/:hospitals/:Financial_Year/skip", use(masterController.skip));

router.get("/:state/:county/:city/:type/:hospitals/:Financial_Year/reports", use(masterController.reports));

router.get("/:state/:county/:city/:type/:hospitals/:Financial_Year/:sheetId", use(masterController.sheetId));

router.get("/report/:pId", use(masterController.search));

router.get("/providers", use(masterController.providers));

router.post("/favorites", use(masterController.favorites));

router.post("/fav", use(masterController.fav));

router.post("/favReports", use(masterController.favReports));

router.post("/deleteDownload/:id", use(masterController.deleteDownloadReport))

// config routes
router.get("/config/signin", use(viewController.config.signin));

router.get("/config", auth.access("master", false, "/config/signin"), use(viewController.config));

router.get("/config/accounts", auth.access("master", false, "/config/signin"), use(viewController.config.accounts));

router.get("/config/users", auth.access("master", false, "/config/signin"), use(viewController.config.users));

// setup
router.get("/setup", use(viewController.setup));

router.get("/setup/database", use(viewController.setup.database));

router.get("/setup/account", use(viewController.setup.account));

router.get("/setup/stripe", use(viewController.setup.stripe));

router.get("/setup/mailgun", use(viewController.setup.mailgun));

router.get("/setup/finish", use(viewController.setup.finish));



module.exports = router;
