CREATE TABLE IF NOT EXISTS account (
  id int(11) NOT NULL AUTO_INCREMENT,
  email varchar(32) NOT NULL DEFAULT '',
  date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  stripe_customer_id varchar(32) DEFAULT '',
  stripe_subscription_id varchar(32) DEFAULT '',
  plan varchar(64) DEFAULT '',
  referrer varchar(128) DEFAULT '',
  active tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS invite (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  invite_id varchar(16) DEFAULT NULL,
  email varchar(64) DEFAULT NULL,
  account_id int(11) DEFAULT NULL,
  date_sent timestamp NULL DEFAULT NULL,
  used tinyint(1) DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS user (
  id int(11) unsigned NOT NULL AUTO_INCREMENT,
  name varchar(32) DEFAULT NULL,
  email varchar(32) NOT NULL DEFAULT '',
  password varchar(64) NOT NULL DEFAULT '',
  date_created timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_login timestamp NULL DEFAULT NULL,
  permission varchar(16) DEFAULT NULL,
  account_id int(11) DEFAULT NULL,
  all_account_ids varchar(100) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY email (email),
  KEY parent_account (account_id),
  CONSTRAINT parent_account FOREIGN KEY (account_id) REFERENCES account (id) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
