const express = require("express");
const bodyParser = require("body-parser");
const hbs = require("express-handlebars");
const session = require("client-sessions");
const router = require("./router");
const api = require("./api");
const view = require("./model/view");
const settings = require("./settings");
const masterdata = require("./model/masterdata");
var url = require('url');

var app = express();
var port = process.env.PORT || 8080;

// config body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// config directories
app.use(express.static('assets'));
app.use(express.static(__dirname + '/'));

// config sessions
app.use(session({

  cookieName: 'session',
  secret: 'gkhsre85454754dghr5',
  duration: 30 * 60 * 1000,
  activeDuration: 5 * 60 * 1000,

}));
// config handlebars view engine
app.engine("hbs", hbs({

  extname: 'hbs',
  defaultLayout: "app",
  partialsDir: [
    'shared/templates/',
    'views/partials/'
  ],
  helpers: {

    is: function (a, b, options) {
      if (a === b) return options.fn(this);
      else return options.inverse(this);
    },

    counter: function (index) {
      return index + 1;
    },

    isOwner: function (userType, options) {
      if (userType == "owner") return options.fn(this);
      else return options.inverse(this);
    },

    isConfig: function (userType, options) {
      if (userType == "config") return options.fn(this);
      else return options.inverse(this);
    },

    selected: function (option, value) {
      if (option == value) return 'selected';
      else return ''
    },

    eachData: function (context, options) {
      var fn = options.fn, inverse = options.inverse, ctx;
      var ret = "";

      if (context && context.length > 0) {
        for (var i = 1, j = context.length; i < j; i++) {
          ctx = Object.create(context[i]);
          ctx.index = i;
          ret = ret + fn(ctx);
        }
      } else {
        ret = inverse(this);
      }
      return ret;
    },

    sum: function (a, b) {
      if (!a == "" && !b == "") {
        var a = parseInt(a);
        var b = parseInt(b);
        return a + b;
      } else if (a == null) {
        return b;
      } else {
        return a;
      }
    },

    slice: function (context, block) {
      var ret = "",
        page = parseInt(block.hash.page) || 1,
        totalPage = parseInt(block.hash.totalpage) || 4,
        pageSize = Math.ceil(context.length / totalPage),
        start = (page - 1) * pageSize,
        end = start + pageSize;
      end = (end > context.length ? context.length : end);
      for (var idx = start; idx < end; idx++) {
        ret += block.fn(context[idx]);
      }
      return ret;
    },

    NumberFormat(num) {
      if (num != null) {
        return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
      }
      return num;
    },

    if_equal: function (a, b, opts) {
      if (a == b) {
        return opts.fn(this)
      } else {
        return opts.inverse(this)
      }
    },

    getNumberClass: function(number) {
      if (number > 0) {
        return 'data'
      } else if (number === 0) {
        return 'data'
      } else {
        return 'negative'
      }
    },

    checkPillActive: function(length, options) {
      if (length > 0) {
        return options.fn(this);
      } else {
        return options.inverse(this);
      }
    }
  }
}));


app.set("view engine", "hbs");

// config routes
app.use(router);
app.use(api);

// // 404 routing
app.use(function (req, res, next) {

  res.render("404.hbs", { content: { title: "404: Page Doesnot exist" } });

});

// error handling
app.use(function (err, req, res, next) {

  console.log(err);

  if (err.sqlMessage)
    res.status(500).send({ message: err.sqlMessage });
  else
    res.status(500).send(err);

});



app.listen(port, function () {

  console.log("Server running on port " + port);

});


