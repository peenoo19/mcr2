const masterdata = require("../model/masterdata");
const auth = require("../model/auth");
const view = require("../model/view");
const account = require("../model/account");
const settings = require("../settings");
const sheetConfig = require("../controller/data-sheet-config")

//List all states
exports.state = async function (req, res) {
  const permission = auth.permission(req);
  const content = await masterdata.state();

  const stateData = {
    0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [], 13: [], 14: [], 15: [], 16: [], 17: [], 18: [], 19: [], 20: [], 21: [], 22: [], 23: [], 24: [], 25: []
  };
  const regEx = [
    /^A/, /^B/, /^C/, /^D/, /^E/, /^F/, /^G/, /^H/, /^I/, /^J/, /^K/, /^L/, /^M/, /^N/, /^O/, /^P/, /^Q/, /^R/, /^S/, /^T/, /^U/, /^V/, /^W/, /^X/, /^Y/, /^Z/
  ];
  let a;
  content.forEach(element => {
    a = regEx.findIndex(reg => {
      return reg.test(element.state);
    });
    if (a !== -1) {
      stateData[a].push(element);
    }
  });

  if (permission) {
    const user = await view.state(req.session.user, req.session.account);
    res.render("state", {
      data: content,
      permission: permission,
      layout: "fixedNavNew",
      content: user,
      stateData: stateData
    });
  }
  else {
    res.render("state", {
      data: content,
      permission: permission,
      layout: "fixedNavNew",
      stateData: stateData
    });
  }
}

//List all county based on the selected state
exports.county = async function (req, res) {
  const permission = auth.permission(req);
  const content = await masterdata.county(req.params.state);

  const countyData = {
    0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [], 13: [], 14: [], 15: [], 16: [], 17: [], 18: [], 19: [], 20: [], 21: [], 22: [], 23: [], 24: [], 25: []
  };
  const regEx = [
    /^A/, /^B/, /^C/, /^D/, /^E/, /^F/, /^G/, /^H/, /^I/, /^J/, /^K/, /^L/, /^M/, /^N/, /^O/, /^P/, /^Q/, /^R/, /^S/, /^T/, /^U/, /^V/, /^W/, /^X/, /^Y/, /^Z/
  ];
  let a;
  content.forEach(element => {
    a = regEx.findIndex(reg => {
      return reg.test(element.county);
    });
    if (a !== -1) {
      countyData[a].push(element);
    }
  });

  if (permission) {
    const user = await view.county(req.session.user, req.session.account);
    res.render("county", {
      data: content,
      permission: permission,
      layout: "fixedNavNew",
      state: req.params.state,
      content: user,
      countyData: countyData
    });
  }
  else {
    res.render("county", {
      data: content,
      layout: "fixedNavNew",
      state: req.params.state,
      countyData: countyData,
      abc: false
    });
  }
}

//List all city based on the selected state and county
exports.city = async function (req, res) {
  const permission = auth.permission(req);
  const content = await masterdata.city(req.params.state, req.params.county);

  const cityData = {
    0: [], 1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: [], 10: [], 11: [], 12: [], 13: [], 14: [], 15: [], 16: [], 17: [], 18: [], 19: [], 20: [], 21: [], 22: [], 23: [], 24: [], 25: []
  };
  const regEx = [
    /^A/, /^B/, /^C/, /^D/, /^E/, /^F/, /^G/, /^H/, /^I/, /^J/, /^K/, /^L/, /^M/, /^N/, /^O/, /^P/, /^Q/, /^R/, /^S/, /^T/, /^U/, /^V/, /^W/, /^X/, /^Y/, /^Z/
  ];
  let a;
  content.forEach(element => {
    a = regEx.findIndex(reg => {
      return reg.test(element.city);
    });
    if (a !== -1) {
      cityData[a].push(element);
    }
  });

  if (permission) {
    const user = await view.city(req.session.user, req.session.account);
    res.render("city", {
      data: content,
      permission: permission,
      layout: "fixedNavNew",
      state: req.params.state,
      county: req.params.county,
      content: user,
      cityData: cityData
    });
  }
  else {
    res.render("city", {
      data: content,
      layout: "fixedNavNew",
      state: req.params.state,
      county: req.params.county,
      cityData: cityData
    });
  }
}

//List hostitals based on the selected state, county and city
exports.hospitals = async function (req, res) {
  const permission = auth.permission(req);
  if (permission) {
    const user = await view.hospitals(req.session.user, req.session.account);
    const accountData = await account.get(req.session.account);
    const plan = await masterdata.plan(accountData.email);
    console.log("P "+plan);
    //lists all available hospitals when there is no plans selected or everything is selected
    if (plan == null || plan == "EVERYTHING" || plan == "EVERYTHING-ANNUAL-1499" || plan == "FREE-PLAN-0") {
      const content = await masterdata.hospitals(req.params.state, req.params.county, req.params.city);
      res.render("hospitals", {
        data: content,
        permission: permission,
        layout: "fixedNavNew",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        content: user,
      });
    }
    //lists only hospitals available in hha_provider_info
    else if (plan == "HHA" || plan == 'HHA-ANNUAL-199') {
      const content = await masterdata.hhahospitals(req.params.state, req.params.county, req.params.city);
      res.render("hospitals", {
        data: content,
        permission: permission,
        layout: "fixedNavNew",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        content: user,
        plan: plan,
      });
    }
    //lists only hospitals available in hospice_provider_info
    else if (plan == "HOSPICE" || plan === 'HOSPICE-ANNUAL-199') {
      debugger;
      const content = await masterdata.hospicehospitals(req.params.state, req.params.county, req.params.city);
      res.render("hospitals", {
        data: content,
        permission: permission,
        layout: "fixedNavNew",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        content: user,
        plan: plan,
      });
    }
    //lists only hospitals available in hospital_provider_info
    else if (plan == "HOSPITAL" || plan === 'HOSPITAL-ANNUAL-499') {
      debugger;
      const content = await masterdata.hospitalhospitals(req.params.state, req.params.county, req.params.city);
      res.render("hospitals", {
        data: content,
        permission: permission,
        layout: "fixedNavNew",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        content: user,
        plan: plan,
      });
    }
    //lists only hospitals available in snf_provider_info
    else if (plan == "SNF" || plan == 'SNF-ANNUAL-499') {
      debugger;
      const content = await masterdata.snfhospitals(req.params.state, req.params.county, req.params.city);
      res.render("hospitals", {
        data: content,
        permission: permission,
        layout: "fixedNavNew",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        content: user,
        plan: plan,
      });
    }
    //lists only hospitals available in renal_provider_info
    else if (plan == "RENAL" || plan == "RENAL-ANNUAL-199") {
      debugger;
      const content = await masterdata.renalhospitals(req.params.state, req.params.county, req.params.city);
      res.render("hospitals", {
        data: content,
        permission: permission,
        layout: "fixedNavNew",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        content: user,
        plan: plan,
      });
    }
  }
  else {
    const content = await masterdata.hospitals(req.params.state, req.params.county, req.params.city);
    res.render("hospitals", {
      data: content,
      layout: "fixedNavNew",
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
    });

  }
}

//Lists the selected hospital profile
exports.Financial_Year = async function (req, res) {
  const permission = auth.permission(req);
  let isLogin;
  const hospitals = req.params.hospitals;
  const hsptl = hospitals.split("-");
  const pId = hsptl[0];
  const name = hsptl[1];
  var data = {};
  var url = settings.domain + "" + req.url;
  var u = url.replace(/%20/g, " ");
  var incomeData = {};
  if (typeof permission === 'undefined') {
    isLogin = false
  } else {
    isLogin = true;
  }
  if (permission) {
    const user = await view.Financial_Year(req.session.user, req.session.account);
    const result = await masterdata.Financial_Year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.data);
    const FYear = await masterdata.FYear(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.data);
    const FYearwithPlan = await masterdata.FYearwithPlan(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.data);
    const accountData = await account.get(req.session.account);
    const plan1 = await masterdata.plan(accountData.email);
    var plan = '';
    if(plan1 == 'FREE-PLAN-0'){
       plan = null;
    }else{
       plan = plan1
    }
    if (plan == null ) {
      const lastYear = await masterdata.getLastYear(req.params.hospitals, req.params.type);
      const recordId = await masterdata.sheetRecordId(req.params.type, req.params.hospitals, lastYear);
      var fav = await masterdata.FindFav(url);
      incomeData = await masterdata.incomeStatement(req.params.type, recordId);
      data = await masterdata.balanceSheetId(req.params.type, recordId);
      res.render("Financial_Year", {
        result: result.data,
        results: result.address,
        data: data,
        incomeData: incomeData,
        FYear: FYear,
        permission: permission,
        layout: "fixedNav",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        type: req.params.type,
        pId: pId,
        name: name,
        content: user,
        plan: plan,
        fav: fav,
        url: url
      });
    } else {
      const recordId = await masterdata.sheetRecordIdwithPlan(req.params.type, req.params.hospitals, FYearwithPlan);
      var incomeDataPlan = [];
      var fav = await masterdata.FindFav(url);
      incomeDataPlan = await masterdata.incomeStatementforPlan(req.params.type, recordId);
      data = await masterdata.balanceSheetIdforPlan(req.params.type, recordId);
      res.render("Financial_Year", {
        result: result.data,
        results: result.address,
        data: data,
        incomeData: incomeDataPlan,
        FYear: FYear,
        permission: permission,
        layout: "fixedNav",
        state: req.params.state,
        county: req.params.county,
        city: req.params.city,
        type: req.params.type,
        pId: pId,
        name: name,
        content: user,
        plan: plan,
        fav: fav,
        url: url
      });
    }
  }
  else {
    const result = await masterdata.Financial_Year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.data);

    const FYear = await masterdata.FYear(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.data);
    res.render("Financial_Year", {
      result: result.data,
      results: result.address,
      FYear: FYear,
      layout: "fixedNav",
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      pId: pId,
      name: name,
      isLogin: isLogin
    });
  }
}

//for users with no plan suggests to purchase a plan
exports.skip = async function (req, res) {
  const permission = auth.permission(req);
  const hospitals = req.params.hospitals;
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  if (permission) {
    const user = await view.reports(req.session.user, req.session.account);
    const content = await masterdata.reports(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year);
    res.render("skip", {
      data: content.data,
      result: content.result,
      results: content.results,
      permission: permission,
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      pId: pId,
      name: name,
      Financial_Year: req.params.Financial_Year,
      content: user,
    });
  }
  else {
    const result = await masterdata.Financial_Year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.data);
    res.render("skip", {
      result: result.data,
      data: result.address,
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      Financial_Year: req.params.Financial_Year,
      pId: pId,
      name: name,
    });
  }
}


//displays list of reports available for that particular year
exports.reports = async function (req, res) {
  const permission = auth.permission(req);
  var url = settings.domain + "" + req.url;
  var u = url.replace(/%20/g, " ");
  const hospitals = req.params.hospitals;
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  if (permission) {
    const user = await view.reports(req.session.user, req.session.account);
    var fav = await masterdata.FindFav(url);

    const accountData = await account.get(req.session.account);
    const plan1 = await masterdata.plan(accountData.email);
    var plan = '';
    if(plan1 == 'FREE-PLAN-0'){
      plan = null;
    }else{
      plan = plan1
    }

    const content = await masterdata.reports(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year);
    res.render("reports", {
      data: content.data,
      result: content.result,
      results: content.results,
      permission: permission,
      layout: "fixedNav",
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      pId: pId,
      name: name,
      Financial_Year: req.params.Financial_Year,
      content: user,
      fav: fav,
      url: url,
      plan:plan
    });
  }
  else {
    const content = await masterdata.reports(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year);
    res.render("reports", {
      data: content.data,
      result: content.result,
      results: content.results,
      layout: "fixedNav",
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      pId: pId,
      name: name,
      Financial_Year: req.params.Financial_Year,
    });
  }
}

//fetches data for the selected report sheet
exports.sheetId = async function (req, res) {
  const permission = auth.permission(req);
  const hospitals = req.params.hospitals;
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  const sheets = sheetConfig.dconfig[req.params.sheetId];
  if (permission) {
    const user = await view.sheetId(req.session.user, req.session.account);
    const accountData = await account.get(req.session.account);
    const plan1 = await masterdata.plan(accountData.email);
    var plan = ''; var download = '';

    const lastYear = await masterdata.getLastYear(req.params.hospitals, req.params.type);
    if(plan1 == 'FREE-PLAN-0'){
      plan = null;
    }else{
      plan = plan1
    }
    if (req.params.Financial_Year == lastYear) {
      download = true;
    }else{
      download = false;
    }
    var data = {
      permission: permission,
      layout: "sheets",
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      pId: pId,
      name: name,
      Financial_Year: req.params.Financial_Year,
      sheetId: req.params.sheetId,
      content: user,
      plan: plan,
      download: download
    };




    //displays data in the sheet for only last year for user with no plans purchased yet
    if (plan == null) {

      if (req.params.Financial_Year == lastYear) {
        //console.log("FARE "+lastYear + ' '+ req.params.Financial_Year)

        data['hospitalInfo'] = await masterdata.hospitalInfo(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
        data['year'] = await masterdata.year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
        const recordId = await masterdata.getRecordId(req.params.type, req.params.hospitals, req.params.Financial_Year);
        for (var idx = 0; idx < sheets.length; idx++) {
          data['data-' + (idx + 1)] = await masterdata.sheetId(req.params.type, recordId, sheets[idx]);
        }
        res.render('../shared/templates/' + (req.params.type).toLowerCase() + '/' + req.params.sheetId, data);
      }
      //if the selected year is not last year for user with no plans display only the empty templates
      else {
        //console.log("FREE "+lastYear + ' '+ req.params.Financial_Year)

        data['hospitalInfo'] = await masterdata.hospitalInfo(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
        data['year'] = await masterdata.year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
        //res.render('../shared/templates/' + (req.params.type).toLowerCase() + '/' + req.params.sheetId, data);
        res.render('../shared/templates/blank' , data);
      }
    }
    else {
      data['hospitalInfo'] = await masterdata.hospitalInfo(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
      data['year'] = await masterdata.Financial_Year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals);
      const recordId = await masterdata.getRecordId(req.params.type, req.params.hospitals, req.params.Financial_Year);
      for (var idx = 0; idx < sheets.length; idx++) {
        data['data-' + (idx + 1)] = await masterdata.sheetId(req.params.type, recordId, sheets[idx]);
      }
      res.render('../shared/templates/' + (req.params.type).toLowerCase() + '/' + req.params.sheetId, data);
    }
  }
  else {
    var data = {
      layout: "sheets",
      state: req.params.state,
      county: req.params.county,
      city: req.params.city,
      type: req.params.type,
      pId: pId,
      name: name,
      Financial_Year: req.params.Financial_Year,
      sheetId: req.params.sheetId,
    };
    const lastYear = await masterdata.getLastYear(req.params.hospitals, req.params.type);
    if (req.params.Financial_Year == lastYear) {
      data['hospitalInfo'] = await masterdata.hospitalInfo(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
      data['year'] = await masterdata.year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.sheetId);
      const recordId = await masterdata.getRecordId(req.params.type, req.params.hospitals, req.params.Financial_Year);
      for (var idx = 0; idx < sheets.length; idx++) {
        data['data-' + (idx + 1)] = await masterdata.sheetId(req.params.type, recordId, sheets[idx]);
      }
        res.render('../shared/templates/' + (req.params.type).toLowerCase() + '/' + req.params.sheetId, data);
      return;
    }else{
        res.render('../shared/templates/blank' , data);
    }
    // data['hospitalInfo'] = await masterdata.hospitalInfo(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.skip, req.params.sheetId);
    // data['year'] = await masterdata.year(req.params.state, req.params.county, req.params.city, req.params.type, req.params.hospitals, req.params.Financial_Year, req.params.skip, req.params.sheetId);

  }
}

//data fetched in the select by facility type dropdown
exports.providers = async function (req, res) {
  const content = await masterdata.providers(req.query.search, req.query.page);
  res.send( content);
}

exports.favorites = async function (req, res) {
  var url = req.headers.referer;
  decodeURI(url);
  var pId = url.split('/')[4];
  const type = await masterdata.findType(pId);
  const content = await masterdata.FavSearchReport(url, pId, type);
  res.send({ results: content });
}


exports.fav = async function (req, res) {
  var url = req.headers.referer;
  decodeURI(url);
  const type = await masterdata.Type(url);
  const content = await masterdata.Fav(url, type);
  res.send({ results: content });
}

exports.favReports = async function (req, res) {

  var url = req.headers.referer;
  var userid = req.session.user;
  if(userid){
    decodeURI(url);
    const type = await masterdata.Type(url);
    const content = await masterdata.saveDownload(url, type, userid);
    res.send({ results: content });
  }
}

exports.deleteDownloadReport = async function (req, res) {

  var id = req.params.id;
  if(id){
    const content = await masterdata.deleteDownload(id);
    res.send({ results: content });
  }
  return
}

//data displayed the dynamic dropdown related to search
exports.search = async function (req, res) {
  const permission = auth.permission(req);
  var data = {};
  var incomeData = {};
  var url = settings.domain + "" + req.url;
  if (permission) {
    const user = await view.search(req.session.user, req.session.account);
    const type = await masterdata.findType(req.params.pId);
    console.log("type", type)
    const content = await masterdata.search(req.params.pId, type);

    const accountData = await account.get(req.session.account);
    const plan = await masterdata.plan(accountData.email);
    if (plan == null || plan == "FREE-PLAN-0") {
      const searchFYear = await masterdata.searchFYear(req.params.pId, type);
      const lastYearSearch = await masterdata.getLastYearSearch(req.params.pId, type);
      const recordIdSearch = await masterdata.sheetRecordIdSearch(type, req.params.pId, lastYearSearch);
      const fav = await masterdata.FindFav(url);
      incomeData = await masterdata.incomeStatement(type, recordIdSearch);
      data = await masterdata.balanceSheetId(type, recordIdSearch);
      res.render("year", {
        result: content.result,
        results: content.results,
        data: data,
        incomeData: incomeData,
        type: type,
        searchFYear: searchFYear,
        pId: req.params.pId,
        layout: "fixedNav",
        permission: permission,
        content: user,
        fav: fav,
        url: url,
        planNull: true
      });
    } else {
      const searchFYear = await masterdata.searchFYear(req.params.pId, type);
      const searchFYearPlan = await masterdata.searchFYearPlan(req.params.pId, type)
      const recordIdSearch = await masterdata.sheetRecordIdSearchwithPlan(type, req.params.pId, searchFYearPlan);
      incomeData = await masterdata.incomeStatementforPlan(type, recordIdSearch);
      const fav = await masterdata.FindFav(url);
      data = await masterdata.balanceSheetIdforPlan(type, recordIdSearch);
      console.log("data", data)
      res.render("year", {
        result: content.result,
        results: content.results,
        data: data,
        incomeData: incomeData,
        type: type,
        searchFYear: searchFYear,
        pId: req.params.pId,
        layout: "fixedNav",
        permission: permission,
        content: user,
        fav: fav,
        url: url,
        planNull: false
      });
    }
  }
  else {
    const type = await masterdata.findType(req.params.pId);
    const content = await masterdata.search(req.params.pId, type);
    const searchFYear = await masterdata.searchFYear(req.params.pId, type);
    res.render("year", {
      result: content.result,
      type: type,
      searchFYear: searchFYear,
      results: content.results,
      layout: "fixedNav",
      pId: req.params.pId,
    });
  }
}


