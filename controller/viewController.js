const settings = require("../settings");
const view = require("../model/view");
const masterdata = require("../model/masterdata");
const auth = require("../model/auth");
const user = require("../model/user");
const userController = require("./userController");
const config = require("../model/config");
const account = require("../model/account");

// website views
exports.home = async function (req, res) {
  const permission = auth.permission(req);
  let authenticated = true;
  let purchasedPlan = {
    HOSPICE: false,
    RENAL: false,
    HHA: false,
    SNF: false,
    HOSPITAL: false,
    EVERYTHING: false
  };

  /* To reset the allInclusive price to 1499 */
  var plans = settings.stripe.plans;
  const inclsivePlanIndex = plans.findIndex(plan => {
    if (plan.id === 'EVERYTHING-ANNUAL-1499' || plan.name === 'EVERYTHING') {
      return plan;
    }
  });
  plans[inclsivePlanIndex].price = '1499 USD';

  if (req.session.permission == "master") {
    const user = await view.home(req.session.user, req.session.account);
    purchasedPlan = await knowPurchasedPlan(purchasedPlan, req);
    res.render("home/home", {
      content: user,
      permission: permission,
      authenticated: authenticated,
      purchasedPlan: purchasedPlan,
      totalInclusivePrice: req.session.totalInclusivePriceOfUser ? req.session.totalInclusivePriceOfUser : false,
      totalDiscount: req.session.totalDiscount ? req.session.totalDiscount : 0,
      layout: "homenew.hbs",
    });
  } else {
    let authenticated = false;

    if (req.session.user && req.session.permission != "master") {
      purchasedPlan = await knowPurchasedPlan(purchasedPlan, req);
      authenticated = true;
    }

    res.render("home/home", {
      authenticated: authenticated,
      purchasedPlan: purchasedPlan,
      totalInclusivePrice: req.session.totalInclusivePriceOfUser ? req.session.totalInclusivePriceOfUser : false,
      totalDiscount: req.session.totalDiscount ? req.session.totalDiscount : 0,
      layout: "homenew.hbs",
    });
  }
}

exports.pricing = async function (req, res) {
  const permission = auth.permission(req);
  let authenticated = true;
  if (req.session.permission == "master") {
    const user = await view.home(req.session.user, req.session.account);
    res.render("home/pricing", {
      content: user,
      permission: permission,
      authenticated: authenticated,
      layout: "homenew.hbs",
    });
  } else {
    let authenticated = false;

    if (req.session.user && req.session.permission != "master")
      authenticated = true;

    res.render("home/pricing", {

      authenticated: authenticated,
      layout: "homenew.hbs",

    });
  }
}

// exports.pricing = (req, res) => {

//   res.render("home/pricing", { layout: "home.hbs" });

// }

exports.terms = (req, res) => {

  res.render("home/terms", { layout: "homenew.hbs" });

}

exports.privacy = (req, res) => {

  res.render("home/privacy", { layout: "homenew.hbs" });

}

exports.refund = (req, res) => {

  res.render("home/refund", { layout: "homenew.hbs" });

}

// auth routes
exports.signup = (req, res) => {

  var plans = settings.stripe.plans;
  var plan = req.query.plan;
  if (plan) {
    var currentIndex = plans.findIndex(o => o.id === plan);
    plans.splice(0, 0, plans.splice(currentIndex, 1)[0]);
  }
  res.render("auth/signup-account", {

    content: {
      title: "Sign up",
      billingInterval: settings.stripe.billingInterval,
      currency: settings.stripe.currencySymbol,
      plans: plans
    },
    layout: "homenew.hbs"

  });
}

exports.buyplan = async (req, res) => {

  if (!req.session.user) {
    res.redirect("/signin");
    return;
  }

  var plans = settings.stripe.plans;
  var plan = req.query.plan;
  if (plan) {
    var currentIndex = plans.findIndex(o => o.id === plan);
    plans.splice(0, 0, plans.splice(currentIndex, 1)[0]);
  }

  if (req.session.totalInclusivePriceOfUser) {
    const inclsivePlanIndex = plans.findIndex(plan => {
      if (plan.id === 'EVERYTHING-ANNUAL-1499' || plan.name === 'EVERYTHING') {
        return plan;
      }
    });
    plans[inclsivePlanIndex].price = req.session.totalInclusivePriceOfUser + ' USD';
  }

  res.render("account/buy-plan", {
    content: {
      title: "Buy Plan",
      billingInterval: settings.stripe.billingInterval,
      currency: settings.stripe.currencySymbol,
      plans: plans
    },
    layout: "homenew.hbs"
  });
};

exports.signup.user = (req, res) => {

  res.render("auth/signup-user", {

    content: { title: "Sign up to MedicareCostReport" },
    layout: "home.hbs"

  });
}

exports.signin = (req, res) => {

  res.render("auth/signin", {
    content: { title: "Sign in to MedicareCostReport" },
    layout: "homenew.hbs"
  });
}

exports.signout = (req, res) => {

  auth.signout(req.session);
  res.redirect("/signin");

}

exports.forgotpassword = (req, res) => {

  res.render("auth/forgot-password", {

    content: { title: "Reset Password" },
    layout: "home",

  });
}

// app routes
exports.dashboard = async (req, res) => {
  const permission = auth.permission(req);
  debugger;
  const content = await view.dashboard(req.session.user, req.session.account);
  const favorites = await masterdata.favorite(req.session.user);
  let purchasedPlans = [];
  let plans= settings.stripe.plans;
  var check = true;
  const plann = '';
  for (i = 0; i < content.planPurchased.length; i++){
    const inclusivePlan = settings.stripe.plans.find(plan => {
      if (plan.id === content.planPurchased[i].plan) {
        if(plan.name == 'EVERYTHING'){
            check = false;
        };
        purchasedPlans.push({
          name: plan.name, price: plan.price.replace('USD', '')
        });
      }
    })
    /*purchasedPlans.push({
      plan: content.planPurchased[i].plan
    });*/
  }
  res.render("dashboard",
      {
        content: content, Hospital: favorites,permission: permission,purchasedPlans:purchasedPlans, check: check
      });

}

exports.account = async (req, res) => {

  const permission = auth.permission(req);

  if (permission.owner) {

    const content = await view.account(req.session.account);
    res.render("account/account-profile", {

      content: content,
      permission: permission

    });
  }

  else if (permission.user) {

    const content = await view.user(req.session.user, req.session.account);
    res.render("account/account-profile", {

      content: content,
      permission: permission

    });
  }
}

exports.account.password = async (req, res) => {

  const permission = auth.permission(req);
  const content = await view.user.password(req.session.user, req.session.account);

  res.render("account/account-password", {

    content: content,
    permission: permission

  });
}

exports.account.billing = async (req, res) => {
  const type = req.query.type;
  const permission = auth.permission(req);
  const accountData = await account.get(req.session.account);
  const plan = await masterdata.plan(accountData.email);
  const content = await view.account.billing(req.session.account);
  var p = false;
  if(plan == 'FREE-PLAN-0'){
    p = true;
  }
  content.plans.forEach(element => {
    if (element.name === type) {
      element.selected = true;
    } else {
      element.selected = false;
    }
  });
  //console.log(content);
  res.render("account/account-billing", {

    content: content,permission: permission,plan:p

  });
}

exports.account.billing.failed = async (req, res) => {

  let domain = settings.domain;
  domain = domain.replace("http://", "");
  domain = domain.replace("https://", "");

  res.render("account/billing-failed", {

    content: {

      title: "Billing",
      domain: domain

    }
  });
}

exports.account.users = async (req, res) => {
  const permission = auth.permission(req);
  const content = await view.account.users(req.session.account, req.session.user);
  res.render("account/account-users", {

    content: content,permission: permission,

  });
}


exports.resetpassword = async (req, res) => {

  await userController.update.password.reset(req, res);

  res.render("auth/reset-password", {

    content: { title: "Password Reset Confirmed" }

  });
}

// config routes
exports.config = async (req, res) => {

  const content = await view.config();

  res.render("config/dashboard", {

    content: content,
    layout: "config",

  });
}

exports.config.signin = (req, res) => {

  res.render("config/signin", {

    content: { title: "Sign in to MedicareCostReport", },
    layout: "config"

  });
}

exports.config.accounts = async (req, res) => {

  const accounts = await config.accounts();

  res.render("config/accounts", {

    content: {

      title: "Accounts",
      accounts: accounts,
      plans: settings.stripe.plans,
      billingInterval: settings.stripe.billingInterval,
      currencySymbol: settings.stripe.currencySymbol

    },

    layout: "config"

  });
}

exports.config.users = async (req, res) => {

  const users = await config.users();

  res.render("config/users", {

    content: {
      title: "Users",
      users: users,
      permissions: settings.permissions
    },

    layout: "config"

  });
}

// setup
exports.setup = (req, res) => {

  res.render("setup/welcome", {

    layout: "config"

  });
}

exports.setup.database = (req, res) => {

  res.render("setup/database", {

    content: { database: settings.database },
    layout: "config"

  });
}

exports.setup.account = (req, res) => {

  res.render("setup/account", {

    layout: "config"

  });
}

exports.setup.stripe = (req, res) => {

  res.render("setup/stripe", {

    content: { stripe: settings.stripe },
    layout: "config"

  });
}

exports.setup.mailgun = (req, res) => {

  res.render("setup/mailgun", {

    content: { mailgun: settings.mailgun },
    layout: "config"

  });
}

exports.setup.finish = (req, res) => {

  res.render("setup/finish", {

    layout: "config"

  });
}

/* To find all purchased plans of user, and subtract the price of these from inclusive plan */
const knowPurchasedPlan = async (purchasedPlan, req) => {
  let userData = await view.accountIdsOfUser(req.session.user);
  let accountIds;
  let totalInclusivePrice;
  let planPrice;
  let perDayAmount;
  let totalDaysLeft;
  let planPriceLeft;
  let expired;
  let totalDiscount = 0;

  const inclusivePlan = settings.stripe.plans.find(plan => {
    if (plan.id === 'EVERYTHING-ANNUAL-1499' || plan.name === 'EVERYTHING') {
      return plan;
    }
  })

  totalInclusivePrice = parseInt(inclusivePlan.price.split(' ')[0]);

  if (!!userData.all_account_ids) {
    accountIds = userData.all_account_ids.split(',');
    const plansOfUser = await view.getAllAccountIdsData(accountIds);
    plansOfUser.forEach(element => {
      if (!!element.plan) {
        /* check plan is expired or not as of 1 year */
        expired = false;
        const createdDate = new Date(element.date_created);
        const todayDate = new Date();
        const timeDifference = todayDate.getTime() - createdDate.getTime();
        var pastDays = Math.floor(timeDifference / (1000 * 3600 * 24));
        if (Math.floor(pastDays) > 365) {
          expired = true;
        }

        if (element.plan == 'HOSPICE-ANNUAL-199' || element.plan == 'HOSPICE') {
          if (expired) {
            purchasedPlan.HOSPICE = false;
          } else {
            purchasedPlan.HOSPICE = true;
          }
        } else if (element.plan == 'RENAL-ANNUAL-199' || element.plan == 'RENAL') {
          if (expired) {
            purchasedPlan.RENAL = false;
          } else {
            purchasedPlan.RENAL = true;
          }
        } else if (element.plan == 'HHA-ANNUAL-199' || element.plan == 'HHA') {
          if (expired) {
            purchasedPlan.HHA = false;
          } else {
            purchasedPlan.HHA = true;
          }
        } else if (element.plan == 'SNF-ANNUAL-499' || element.plan == 'SNF') {
          if (expired) {
            purchasedPlan.SNF = false;
          } else {
            purchasedPlan.SNF = true;
          }
        } else if (element.plan == 'HOSPITAL-ANNUAL-499' || element.plan == 'HOSPITAL') {
          if (expired) {
            purchasedPlan.HOSPITAL = false;
          } else {
            purchasedPlan.HOSPITAL = true;
          }
        } else if (element.plan == 'EVERYTHING-ANNUAL-1499' || element.plan == 'EVERYTHING') {
          if (expired) {
            purchasedPlan.EVERYTHING = false;
            purchasedPlan.HOSPICE = false;
            purchasedPlan.RENAL = false;
            purchasedPlan.HHA = false;
            purchasedPlan.SNF = false;
            purchasedPlan.HOSPITAL = false;
          } else {
            purchasedPlan.EVERYTHING = true;
            purchasedPlan.HOSPICE = true;
            purchasedPlan.RENAL = true;
            purchasedPlan.HHA = true;
            purchasedPlan.SNF = true;
            purchasedPlan.HOSPITAL = true;
          }
        }

        if (!purchasedPlan.EVERYTHING) {
          // get each bought plans of user
          const planData = settings.stripe.plans.find(plan => {
            if (plan.id === element.plan || plan.name === element.plan) {
              return plan;
            }
          });

          var date1 = new Date(element.date_created);
          var date2 = new Date();

          var Difference_In_Time = date2.getTime() - date1.getTime();

          // To calculate the no. of days between two dates and price to subtract
          var Difference_In_Days = Math.floor(Difference_In_Time / (1000 * 3600 * 24));

          planPrice = parseInt(planData.price.split(' ')[0]);
          perDayAmount = planPrice / 365;
          totalDaysLeft = 365 - Difference_In_Days;
          planPriceLeft = Math.floor(totalDaysLeft * perDayAmount);
          totalInclusivePrice = totalInclusivePrice - planPriceLeft;
          totalDiscount += planPriceLeft;
        }

      }
    });
  }
  req.session.totalInclusivePriceOfUser = totalInclusivePrice;
  req.session.totalDiscount = totalDiscount;
  if (req.session.totalInclusivePriceOfUser < 0) {
    req.session.totalInclusivePriceOfUser = 1499;
  }

  if (purchasedPlan.EVERYTHING) {
    req.session.totalInclusivePriceOfUser = 1499;
  }
  return purchasedPlan;
};
