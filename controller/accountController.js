const account = require("../model/account");
const user = require("../model/user");
const auth = require("../model/auth");
const stripe = require("../model/stripe");
const mail = require("../model/mail");
const authController = require("../controller/authController");

/*
* create a new account and stripe customer
*/

exports.create = async function(req, res){

  const signup = req.body;
  let stripeData = {};
  let planPrice

  planPrice = parseInt(signup.plan.split('-')[2]);

  console.log("Plan "+planPrice)
  // check if the account already exists
  const exists = await account.get(null, signup.email);

  if (exists)
    throw ({ inputError: "email", message: "You're already registered" });

  // do not create stripe account. as there is no credit card detail to submit
  if(signup.plan == 'FREE-PLAN-0'){
    stripeData.customer = {id: ""};
    stripeData.subscription = {id: ""};
  }else{
    // create a stripe customer and subscribe them to a plan
    stripeData.customer = await stripe.customer.create(signup.email, signup.source);
    stripeData.subscription = await stripe.customer.subscribe(stripeData.customer.id, signup.plan);
  }


  // create the account and user
  const accountId = await account.create(signup, stripeData);
  //console.log(accountId);
  const userId = await user.create(signup.name, signup.email, signup.password, accountId, "owner");
  const purchasePlan = await user.createUserPurchasePlan(userId, accountId, signup.plan, planPrice);

  // // send welcome email
  // mail.send(signup.email, "Welcome to MedicareCostReport!", "welcome-account", {
  //   name: signup.name, plan: signup.plan, price: stripeData.subscription.price
  // });

  // autheticate user
  authController.signin(req, res);

};

/* create a new account and stripe customer for already logged in user */

exports.createAccount = async function(req, res) {
  const planData = req.body;
  let stripeData = {};
  let planPrice

  if (planData.plan === 'EVERYTHING-ANNUAL-1499' || planData.plan === 'EVERYTHING') {
    planPrice = req.session.totalInclusivePriceOfUser;
  } else {
    planPrice = parseInt(planData.plan.split('-')[2]);
  }
  if(planPrice == null){
    planPrice = parseInt(planData.plan.split('-')[2]);
  }

  const userData = await user.getUserByID(req.session.user);

  planData.email = userData.email;

  // create a stripe customer and subscribe them to a plan
  stripeData.customer = await stripe.customer.create(userData.email, planData.source);
  stripeData.subscription = await stripe.customer.subscribe(stripeData.customer.id, planData.plan);

  const accountId = await account.create(planData, stripeData);
  if(accountId != 0){
    const updatedUser = await user.updateAccountId(req.session.user, userData.all_account_ids, accountId);
    const purchasePlan = await user.createUserPurchasePlan(userData.id, accountId, planData.plan, planPrice);
    if (updatedUser.status === 200) {
      res.send({ status: 200, message: "You are subscribed to the plan" });
    }
  }else{
    const purchasePlan = await user.createUserPurchasePlan(userData.id, accountId, planData.plan, planPrice);
    res.send({ status: 200, message: "You are subscribed to the plan" });
  }


};

/*
* update the account profile (name & email)
*/

exports.update = async function(req, res){

  const profile = {

    email: req.body.email,
    name: req.body.name

  };

  // update the account & user
  await account.update(req.session.account, profile);
  await user.update(req.session.user, req.session.account, profile);
  res.send({ status: 200, message: "Your profile has been updated" });

};

/*
* upgrade or downgrade the billing plan
*/

exports.update.plan = async function(req, res){

  const accountData = await account.get(req.account);

  // update stripe
  let subscription = await stripe.subscription(accountData.stripe_subscription_id);
  subscription = await stripe.subscription.update(subscription, req.body.plan);

  // update the database
  account.update.plan(accountData.id, req.body.plan);

  // email the account holder
  // mail.send(accountData.email, "Your billing plan has been updated", "plan-updated", {
  //   name: accountData.name, plan: subscription.items.data[0].plan.nickname
  // });

  res.send({ status: 200, message: "Your plan has been updated" });

};

/*
* update credit card defails
*/

exports.update.card = async function(req, res){

  const accountData = await account.get(req.account)
  await stripe.customer.update(accountData.stripe_customer_id, req.body.source);
  res.send({ status: 200, message: "Your card details have been updated" });

};

/*
* close the account and delete all users associated with it
*/

exports.close = async function(req, res){

  const accountData = await account.get(req.account);
  await stripe.customer.delete(accountData.stripe_customer_id);
  await account.delete(accountData.id);

  mail.send(accountData.email, "We're sorry to see you go", "close-account", {
    name: accountData.name });

  auth.signout(req.session);
  res.send({ status: 200 });

};
