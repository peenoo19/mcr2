const config = require("../model/config");
const account = require("../model/account");
const user = require("../model/user");
const mail = require("../model/mail");
const stripe = require("../model/stripe");

/*
* configure the database tables
*/

exports.database =  async function(req, res){

  // save the database settings
  await config.settings("database", req.body);
  await config.database();
  res.send({ status: 200 })

};

/*
* create the master account
*/

exports.account = async function(req, res){

  const accountData = await account.get(null, req.body.email);

  if (accountData)
    throw ({ success: false, message: "You've already registered an account" });

  // format data for account creation
  const newAccountData = {

    email: req.body.email,
    plan: null,
    referrer: null,

  };

  const stripeData = {

    subscription: { id: null },
    customer: { id: null }

  }

  // create the account and user
  const accountId = await account.create(newAccountData, stripeData);
  const userId = await user.create("Master", req.body.email, req.body.password, accountId, "master");

  // authenticate user
  req.session.account = accountId;
  req.session.user = userId;
  req.session.permission = "master";
  res.send({ status: 200 });

};

/*
* close an account and delete all of it's users
*/

exports.account.close = async function(req, res){

  const accountData = await account.get(req.body.accountId);
  await stripe.customer.delete(accountData.stripe_customer_id);
  await account.delete(accountData.id);

  mail.send(accountData.email, "We're sorry to see you go", "close-account", {
    name: accountData.name });

  res.send({

    status: 200,
    message: "Account closed",
    callback: "deleteTableRow",
    callbackParams: req.body.accountId

  });
}

/*
* update an account profile
*/

exports.account.update = async function(req, res){

  const newAccountData = req.body;
  newAccountData.active = parseInt(newAccountData.active);

  const accountData = await account.get(newAccountData.id);
  newAccountData.name = accountData.name;

  // update the plan
  if (accountData.plan !== newAccountData.plan){

    // update stripe
    let subscription = await stripe.subscription(accountData.stripe_subscription_id);
    subscription = await stripe.subscription.update(subscription, req.body.plan);

    // update the database
    await account.update.plan(newAccountData.id, newAccountData.plan);

    // email the account holder
    mail.send(accountData.email, "Your billing plan has been updated", "plan-updated", {
      name: accountData.name, plan: subscription.items.data[0].plan.nickname
    });
  }

  // deactivate account
  if (accountData.active !== newAccountData.active)
    await account.activate(newAccountData.id, newAccountData.active)

  // update the email
  if (accountData.email !== newAccountData.email)
    await account.update(accountData.id, newAccountData);

  res.send({

    status: 200,
    message: "Account updated",
    callback: "updateAccountsTable",
    callbackParams: {

      id: newAccountData.id,
      email: newAccountData.email,
      plan: newAccountData.plan,
      active: newAccountData.active

    }
  });
}

/*
* update a user profile
*/

exports.user = async function(req, res){

  const profile = req.body;
  await user.update(profile.id, null, profile, req.permission);

  res.send({

    status: 200,
    message: profile.email + " has been updated",
    callback: "updateUserTable",
    callbackParams: {

      id: profile.id,
      email: profile.email,
      name: profile.name,
      permission: profile.permission

    }
  });
}

/*
* delete a user
*/

exports.user.delete = async function(req, res){

  let msg = await user.delete(req.body.userId, null, req.permission);
  res.send(msg);

}

/*
* setup stripe
*/

exports.stripe = async function(req, res){

  const msg = await config.settings("stripe", req.body);
  config.stripeKey(req.body.publishableAPIKey);
  res.send({ status: 200, message: "Stripe settings updated" });

};

/*
* setup mailgun
*/

exports.mailgun = async function(req, res){

  const msg = await config.settings("mailgun", req.body);
  res.send({ status: 200, message: "Mailgun settings updated" })

};
