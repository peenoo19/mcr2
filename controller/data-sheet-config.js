var dconfig = [];
dconfig['A000000'] = ['A000000'];
dconfig['A000000-partA'] = ['A000000'];
dconfig['A000000-partB'] = ['A000000'];
dconfig['A100000'] = ['A100000'];
dconfig['A200000'] = ['A200000'];
dconfig['A300000'] = ['A300000'];
dconfig['A400000'] = ['A400000'];
dconfig['A500000'] = ['A500000'];
dconfig['A600000'] = ['A600000'];
dconfig['A60000A-B-C'] = ['A60000A','A60000B','A60000C'];
dconfig['A700001-to-3'] = ['A700001','A700002','A700003'];
dconfig['A700000'] = ['A700000'];
dconfig['A700001'] = ['A700001'];
dconfig['A800000'] = ['A800000'];
dconfig['A810000'] = ['A810000'];
dconfig['A810001-2'] = ['A810001','A810002'];
dconfig['A810000-partA'] = ['A810000'];
dconfig['A810000-partB'] = ['A810000'];
dconfig['A820000'] = ['A820000'];
dconfig['A830000'] = ['A830000'];
dconfig['A830001-to-3'] = ['A830000'];
dconfig['A830004-5'] = ['A830000'];
dconfig['S000001-to-3'] = ['S000001','S000002','S000003'];
dconfig['S000001-2'] = ['S000001','S000002'];
dconfig['S000000'] = ['S000000'];
dconfig['S100000'] = ['S100000'];
dconfig['S100001'] = ['S100001'];
dconfig['S100002-3'] = ['S100002','S100003'];
dconfig['S200000-partA'] = ['S200000'];
dconfig['S200000-partB'] = ['S200000'];
dconfig['S200000'] = ['S200000'];
dconfig['S200001'] = ['S200001'];
dconfig['S200002'] = ['S200002'];
dconfig['S300001'] = ['S300001'];
dconfig['S300001-to-3'] = ['S300002', 'S300002', 'S300003'];
dconfig['S300002-to-3'] = ['S300002', 'S300003'];
dconfig['S300004'] = ['S300004'];
dconfig['S300005'] = ['S300005'];
dconfig['S400000'] = ['S400000'];
dconfig['S500000'] = ['S500000'];
dconfig['S700000'] = ['S700000'];
dconfig['B000000'] = ['B000000'];
dconfig['B000000-partA'] = ['B000000'];
dconfig['B000000-partB'] = ['B000000'];
dconfig['B000000-partC'] = ['B000000'];
dconfig['B000000-partD'] = ['B000000'];
dconfig['B100000-partA'] = ['B100000'];
dconfig['B100000-partB'] = ['B100000'];
dconfig['B100000-partC'] = ['B100000'];
dconfig['B100000-partD'] = ['B100000'];
dconfig['B000001'] = ['B000001'];
dconfig['B100000'] = ['B100000'];
dconfig['B200000'] = ['B200000'];
dconfig['B000002'] = ['B000002'];
dconfig['B200010'] = ['B200010'];
dconfig['B1000000'] = ['B1000000'];
dconfig['C000000'] = ['C000000'];
dconfig['C000001'] = ['C000001'];
dconfig['C000001-2'] = ['C000001','C000002'];
dconfig['C000003-to-5'] = ['C000003','C000004','C000005'];
dconfig['D000000'] = ['D000000'];
dconfig['D100000'] = ['D100000'];
dconfig['D000001'] = ['D000001'];
dconfig['D000002-to-3'] = ['D000002', 'D000003'];
dconfig['E000001'] = ['E000001'];
dconfig['E100000'] = ['E100000'];
dconfig['E000001-2'] = ['E000001','E000002'];
dconfig['F000000'] = ['F000000'];
dconfig['F100000'] = ['F100000'];
dconfig['F200000'] = ['F200000'];
dconfig['G000000'] = ['G000000'];
dconfig['O100000'] = ['O100000'];
dconfig['G200001-to-2'] = ['G200001','G200002'];
dconfig['G100000'] = ['G100000'];
dconfig['G200000'] = ['G200000'];
dconfig['G300000'] = ['G300000'];
dconfig['H010000'] = ['H010000'];
dconfig['K010000'] = ['K010000'];
dconfig['K110000'] = ['K110000'];
dconfig['K210000'] = ['K210000'];
dconfig['K310000'] = ['K310000'];
dconfig['K410001'] = ['K410001'];
dconfig['K410002'] = ['K410002'];
dconfig['K510001'] = ['K510001'];
dconfig['K510002'] = ['K510002'];
dconfig['K510003'] = ['K510003'];
dconfig['K610000'] = ['K610000'];
dconfig['O100000'] = ['0100000'];
dconfig['O200000'] = ['0200000'];
dconfig['O300000'] = ['0300000'];
dconfig['O400000'] = ['0400000'];
dconfig['O500000'] = ['0500000'];



exports.dconfig = dconfig;