const metrics = require("../model/metrics");

exports.growth = async function(req, res){

  const chartData = await metrics.growth();
  res.send({ status: 200, chart: chartData });

}

exports.accounts = async function(req, res){

  const totalAccounts = await metrics.accounts();
  res.send({ status: 200, totalAccounts: totalAccounts })

}

exports.accounts.active = async function(req, res){

  const totalActiveAccounts = await metrics.accounts.active();
  res.send({ status: 200, totalActiveAccounts: totalActiveAccounts });

}

exports.accounts.churned = async function(req, res){

  const totalChurnedAccounts = await metrics.accounts.churned();
  res.send({ status: 200, totalChurnedAccounts: totalChurnedAccounts });

}
