const auth = require("../model/auth");
const account = require("../model/account");
const user = require("../model/user");
const stripe = require("../model/stripe");

/*
* authenticate user
*/

exports.signin = async function(req, res, next){
  
  let signin = req.body;

  // check user exists
  const userData = await user.get(null, signin.email);
  console.log(userData);

  if (!userData)
    throw ({ status: false, inputError: "email", message: "Email address isn't registered" });

  // verify  password
  const verified = await user.password.verify(userData.id, userData.account_id, signin.password);

  if (!verified)
    throw ({ status: false, inputError: "password", message: "Please enter the correct login details" })

  const accountData = await account.get(userData.account_id);

  // set permission
  req.session.account = userData.account_id;
  req.session.user = userData.id;
  req.session.permission = userData.permission;
  var token = await auth.token(accountData.id, userData.id, req.session.permission);
  req.session.token = token;

  // check for an active stripe subscription
  if (req.session.permission != "master"){

    let subscription = await stripe.subscription(accountData.stripe_subscription_id);


    if (subscription.status == "active"){

      account.activate(accountData.id, true);
      req.session.subscription = true;

    }
    else {

      account.activate(accountData.id, false);
      req.session.subscription = false;

    }
  }
  else
    req.session.subscription = true;

   // update last login and create the token
   user.update.lastlogin(userData.id, userData.account_id);
   //const token = await auth.token(accountData.id, userData.id, req.session.permission);
   res.send({ status: 200, token: token });
};
