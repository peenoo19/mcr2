const user = require("../model/user");
const account = require("../model/account");
const mail = require("../model/mail");
const randomstring = require("randomstring");
const settings = require("../settings");
const authController = require("../controller/authController");

/*
* create a new user
*/

exports.create = async function(req, res){

  const signup = req.body;

  // check the invite is valid
  const invite = await user.invite(signup.inviteId, signup.email);

  if (!invite)
    throw ({ message: "Invalid invite. Please contact the account holder" });

  // check if the user exists
  const exists = await user.get(null, signup.email);
  if (exists) throw ({ message: "You're already registered" });

  // create the user
  const userId = await user.create(signup.name, signup.email, signup.password, invite.account_id, "user");
  const userData = await user.get(null, signup.email);
  const accountData = await account.get(userData.account_id);

  // close the invite
  user.invite.close(signup.inviteId);

  // send welcome email to user and notifiy account holder
  mail.send(userData.email, "Welcome to MedicareCostReport!", "welcome-user", { name: userData.name });
  mail.send(accountData.email, userData.name + " accepted your invite", "invite-accepted", { name: userData.name });

  // authenticate the user
  authController.signin(req, res);

};

/*
* invite a new user
*/

exports.invite = async function(req, res){

  const accountData = await account.get(req.account);

  // split emails
  const emails = req.body.email.replace(" ", "").split(",");

  // check length
  if (emails.length > 10)
    res.status(500).send({ inputError: "email", message: "Max 10 emails per invite" });

  // invite each user
  for (i = 0; i < emails.length; i++){

    const inviteURL = await user.invite.create(emails[i], req.account);

    mail.send(emails[i], "You've been invited to MedicareCostReport", "user-invite", {

      name: accountData.name,
      email: accountData.email,
      inviteURL: inviteURL,

    });
  }

  // return success message
  if (emails.length > 1) msg = "Invites sent";
  else msg = "Invite sent"

  res.send({ status: 200, message: msg });

};

/*
* update a user profile (name and email by default)
*/

exports.update = async function(req, res){

  let userId;

  // admin is updating user
  if (req.body.userId){

    // check the permission being set exists + prevent master permission being set
    if (settings.permissions.indexOf(req.body.permission) > -1){

      const data = await user.update(req.body.userId, req.account, req.body, req.permission);
      res.send(data);

    }
    else {

      throw ({ status: false, message: "Please specify a valid permission type" });

    }
  }

  // user is updating themselves
  else {

    const data = await user.update(req.user, req.account, req.body, req.permission);
    res.send(data);

  }
};

/*
* update password
*/

exports.update.password = async function(req, res){

  // verify old password & save
  const verified = await user.password.verify(req.user, req.account, req.body.oldpassword);

  if (!verified)
    throw ({ status: false, inputError: "oldpassword", message: "Please enter the correct password" });

  await user.password.save(req.user, req.body.newpassword, false);
  res.send({ success: true, message: "Your new password has been saved" });

}

/*
* request a new password
*/

exports.update.password.forgotten = async function(req, res){

  mail.send(req.body.email, "You requested a password reset", "reset-password",
    { email: req.body.email });

  res.send({ success: true,
    message: "Your new password is on its way, please check your email" });

};

/*
* reset the password
*/

exports.update.password.reset = async function(req, res){

  // generate a random password
  const tempPassword = randomstring.generate(32);
  const userData = await user.get(null, req.query.email);

  // save new password and email the user
  await user.password.save(userData.id, tempPassword, true);
  mail.send(userData.email, "Your temporary password", "temp-password", { password: tempPassword });

}

/*
* delete the user
*/

exports.delete = async function(req, res){

  let msg = await user.delete(req.body.userId, req.account);
  res.json(msg);

};
