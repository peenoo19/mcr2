// set default options for each chart
var chartOptions = {

  scales: {
    xAxes: [{
      gridLines: {
        drawOnChartArea: false,
        color: "rgba(0, 0, 0, 0)",
        zeroLineColor: "transparent"
      },

      ticks: {
        fontFamily: "Source Sans Pro",
        fontSize: 10,
        maxRotation: 90,
      }
    }],

    yAxes: [{
      gridLines: {
        drawOnChartArea: false,
        color: "rgba(0, 0, 0, 0)",
        zeroLineColor: "transparent"
      },

      ticks: {
        fontFamily: "Source Sans Pro",
        fontSize: 10,
        beginAtZero: false,

        // show labels as full numbers (no decimals)
        callback: function(label, index, labels){
          if (Math.floor(label) === label){
            return label;
          }
        }
      }
    }]
  },

  legend: {

    display: false

  },

  defaultFontSize: 11,

  tooltips: {

    backgroundColor: "#404958",
    titleFontSize: 12,
    titleMarginBottom: 6,
    xPadding: 10,
    yPadding: 8,
    enabled: false, // using custom
    displayColors: false,
    callbacks: {
      title: function(){}
    },
    // user custom html tooltip
    custom: function(tooltipModel){

      var tooltipEl = document.getElementById('chart-tooltip');

      // ceate tooltip on first render
      if (!tooltipEl) {
        tooltipEl = document.createElement('div');
        tooltipEl.id = 'chart-tooltip';
        tooltipEl.innerHTML = "<table></table>";
        document.body.appendChild(tooltipEl);
      }

      // hide if no tooltip
      if (tooltipModel.opacity === 0){
        tooltipEl.style.opacity = 0;
        return;
      }

      // set caret Position
      tooltipEl.classList.remove('above', 'below', 'no-transform');
      if (tooltipModel.yAlign) {
        tooltipEl.classList.add(tooltipModel.yAlign);
      }
      else {
        tooltipEl.classList.add('no-transform');
      }

      function getBody(bodyItem) {
        return bodyItem.lines;
      }

      // Set Text
      if (tooltipModel.body) {

        var titleLines = tooltipModel.title || [];
        var bodyLines = tooltipModel.body.map(getBody);

        var innerHtml = '<thead>';

        titleLines.forEach(function(title) {
          innerHtml += '<tr><th>' + title + '</th></tr>';
        });

        innerHtml += '</thead><tbody>';

        bodyLines.forEach(function(body, i) {
          var colors = tooltipModel.labelColors[i];
          var style = 'background:' + colors.backgroundColor;
          style += '; border-color:' + colors.borderColor;
          style += '; border-width: 2px';
          var span = '<span style="' + style + '"></span>';
          innerHtml += '<tr><td>' + span + body + '</td></tr>';
        });

        innerHtml += '</tbody>';

        var tableRoot = tooltipEl.querySelector('table');
        tableRoot.innerHTML = innerHtml;

      }

      var position = this._chart.canvas.getBoundingClientRect();
      tooltipEl.style.opacity = 1;
      tooltipEl.style.position = 'fixed';
      tooltipEl.style.left = position.left + tooltipModel.caretX + 'px';
      tooltipEl.style.top = position.top + tooltipModel.caretY + 'px';
      tooltipEl.style.fontFamily = tooltipModel._bodyFontFamily;
      tooltipEl.style.fontSize = tooltipModel.bodyFontSize + 'px';
      tooltipEl.style.fontStyle = tooltipModel._bodyFontStyle;
      tooltipEl.style.padding = tooltipModel.yPadding + 'px ' + tooltipModel.xPadding + 'px';

    }
  }
};

// chart colours
var blueChart = {

  borderColor: "#77AFE8",
  backgroundColor: ["#77AFE8", "#79B3EF", "#8DC2F9", "#ACD3FB", "#A9D3FE", "#C5E1FD"],
  transparentColor: "rgba(119, 175, 232, 0.1)",
  pointRadius: 4,
  pointHoverRadius: 5,
  pointBorderWidth: 2,
  pointBackgroundColor: "#FFFFFF",
  pointHoverBackgroundColor: "#FFFFFF",
  pointHoverBorderColor: "#77AFE8"

};

var greenChart = {

  borderColor: "#75CD9F",
  backgroundColor: ["#75CD9F", "#82D7AB", "#A0E8C2", "#A8EDC8", "#A7EBC8"],
  transparentColor: "rgba(134, 206, 201, 0.1)",
  pointRadius: 4,
  pointHoverRadius: 5,
  pointBorderWidth: 2,
  pointBackgroundColor: "#FFFFFF",
  pointHoverBackgroundColor: "#FFFFFF",
  pointHoverBorderColor: "#75CD9F",

};

var purpleChart = {

  borderColor: "#8D8CC3",
  backgroundColor: ["#8D8CC3", "#9A99CB", "#A6A5D7", "#B6B5E4", "#C5C4F1", "#CECDF8"],
  transparentColor: "rgba(166, 165, 207, 0.1)",
  pointRadius: 4,
  pointHoverRadius: 5,
  pointBorderWidth: 2,
  pointBackgroundColor: "#FFFFFF",
  pointHoverBackgroundColor: "#FFFFFF",
  pointHoverBorderColor: "#8D8CC3",

};

var redChart = {

  borderColor: "#F9627A",
  backgroundColor: ["#F9627A", "#F67489", "#F68697", "#F78C9D", "#F69EAC", "#FBACB9"],
  transparentColor: "rgba(232, 130, 164, 0.1)",
  pointRadius: 4,
  pointHoverRadius: 5,
  pointBorderWidth: 2,
  pointBackgroundColor: "#FFFFFF",
  pointHoverBackgroundColor: "#FFFFFF",
  pointHoverBorderColor: "#F9627A",

};

/*
* setChartColors()
* format the colors for each chart type
* chartType: "line", "sparkline", "pie" or "donut"
* data: the datasets for the chart
* color: blueChart, greenChart, redChart or purpleChart
*/

function setChartColors(chartType, data, color){

  for (var i = 0; i < data.datasets.length; i++){

    data.datasets[i].borderColor = color[i].borderColor;
    data.datasets[i].backgroundColor = color[i].backgroundColor[i];

    if (chartType === "line"){
      data.datasets[i].pointBackgroundColor = color[i].pointBackgroundColor;
      data.datasets[i].backgroundColor = color[i].transparentColor;
      data.datasets[i].pointRadius = color[i].pointRadius;
      data.datasets[i].pointHoverRadius = color[i].pointHoverRadius;
      data.datasets[i].pointBorderWidth = color[i].pointBorderWidth;
      data.datasets[i].pointBackgroundColor = color[i].pointBackgroundColor;
      data.datasets[i].pointHoverBackgroundColor = color[i].pointHoverBackgroundColor;
      data.datasets[i].pointHoverBorderColor = color[i].pointHoverBorderColor;
    }

    if (chartType === "sparkline"){
      data.datasets[i].backgroundColor = "transparent";
      data.datasets[i].pointRadius = 0;
      data.datasets[i].lineTension = 0;
    }

    if (chartType === "pie" || chartType === "donut"){
      data.datasets[i].borderColor = "#FFFFFF";
      data.datasets[i].hoverBorderColor = "transparent";
      data.datasets[i].backgroundColor = color[i].backgroundColor;
    }
  }
}

/*
* createLineChart()
* draw a line chart on the canvas
* chart: class name of the canvas parent wrapper
* data: datasets for the chart
* color: color set for the chart defined above, eg. blueChart
* showLegend: true/false – requires .legend element before canvas wrapper
* useMetrics: true/fase – converts values to metric, eg. 10,000 becomes 10k
*/

function createLineChart(chart, data, color, showLegend, useMetric){

  if (data.datasets[0].data.length < 1){
    $(chart).parent().find(".message").removeClass("hide");
    return false;
  }

  // clone options
  var lineOptions = jQuery.extend(true, {}, chartOptions);
  var lineData = jQuery.extend(true, {}, data);

  // config sizing
  lineOptions.maintainAspectRatio = false;
  lineOptions.responsive = true;

  // config metric labels
  if (useMetric){
    lineOptions.scales.yAxes[0].ticks.callback = function(value){
      return formatToMetric(value, currency);
    };
  }

  setChartColors("line", lineData, color);

  // remove loader
  $(chart).parent().find(".loader").remove();

  // plot chart
  var lineChart = new Chart($("canvas", chart), {
    type: 'line',
    data: lineData,
    options: lineOptions
  });

  // create legend
  if (showLegend){
    $(chart).parent().find(".legend").html(lineChart.generateLegend()).show();
  }
}

/*
* createSparklineChart()
* draw a sparkline chart on the canvas
* chart: class name of the canvas parent wrapper
* data: datasets for the chart
* color: color set for the chart defined above, eg. blueChart
* showLegend: true/false – requires .legend element before canvas wrapper
*/

function createSparklineChart(chart, data, color, showLegend){

  if (data.datasets[0].data.length < 1){
    $(chart).parent().find(".message").removeClass("hide");
    return false;
  }

  // clone options
  var sparkOptions = jQuery.extend(true, {}, chartOptions);
  var sparkData = jQuery.extend(true, {}, data);

  // hide axes
  sparkOptions.scales = {
    xAxes: [{ display: false }],
    yAxes: [{ display: false }]
  };

  // config sizing
  sparkOptions.maintainAspectRatio = false;
  sparkOptions.responsive = true;

  setChartColors("sparkline", sparkData, color);

  // remove loader
  $(chart).parent().find(".loader").remove();

  // plot chart
  var sparkChart = new Chart($("canvas", chart), {
    type: 'line',
    data: sparkData,
    options: sparkOptions
  });
}

/*
* createBarChart()
* draw a bar chart on the canvas
* chart: class name of the canvas parent wrapper
* data: datasets for the chart
* color: color set for the chart defined above, eg. blueChart
* showLegend: true/false – requires .legend element before canvas wrapper
* useMetrics: true/fase – converts values to metric, eg. 10,000 becomes 10k
*/

function createBarChart(chart, data, color, showLegend, useMetric){

  if (data.datasets[0].data.length < 1){
    $(chart).parent().find(".message").removeClass("hide");
    return false;
  }

  // clone objects
  var barOptions = jQuery.extend(true, {}, chartOptions);
  var barData = jQuery.extend(true, {}, data);

  // config sizing
  barOptions.maintainAspectRatio = false;
  barOptions.responsive = true;

  // config metric labels
  if (useMetric){
    barOptions.scales.yAxes[0].ticks.callback = function(value){
      return formatToMetric(value, currency);
    };
  }

  setChartColors("bar", barData, color);

  // remove loader
  $(chart).parent().find(".loader").remove();

  // plot chart
  var barChart = new Chart($("canvas", chart), {
    type: 'bar',
    data: barData,
    options: barOptions
  });

  // create legend
  if (showLegend){
    $(chart).parent().find(".legend").html(barChart.generateLegend()).show();
  }
}

/*
* createPieChart()
* draw a pie chart on the canvas
* chart: class name of the canvas parent wrapper
* data: datasets for the chart
* color: color set for the chart defined above, eg. blueChart
* showLegend: true/false – requires .legend element before canvas wrapper
*/

function createPieChart(chart, data, color, showLegend){

  if (data.datasets[0].data.length < 1){
    $(chart).parent().find(".message").removeClass("hide");
    return false;
  }

  // clone options
  var pieOptions = jQuery.extend(true, {}, chartOptions);
  var pieData = jQuery.extend(true, {}, data);

  // config sizing
  pieOptions.responsive = true;
  pieOptions.maintainAspectRatio = false;

  // hide ticks
  pieOptions.scales.yAxes[0].ticks = { display: false };
  pieOptions.scales.xAxes[0].ticks = { display: false };
  pieOptions.scales.xAxes[0] = { display: false };
  pieOptions.scales.yAxes[0] = { display: false };

  setChartColors("pie", pieData, color);

  // remove loader
  $(chart).parent().find(".loader").remove();

  // plot chart
  var pieChart = new Chart($("canvas", chart), {
    type: 'pie',
    data: pieData,
    options: pieOptions
  });

  // create legend
  if (showLegend){
    $(chart).parent().find(".legend").html(pieChart.generateLegend()).show();
  }
}

/*
* createDonutChart()
* draw a donut chart on the canvas
* chart: class name of the canvas parent wrapper
* data: datasets for the chart
* color: color set for the chart defined above, eg. blueChart
* showLegend: true/false – requires .legend element before canvas wrapper
*/

function createDonutChart(chart, data, color, showLegend){

  if (data.datasets[0].data.length < 1){
    $(chart).parent().find(".message").removeClass("hide");
    return false;
  }

  // clone options
  var donutOptions = jQuery.extend(true, {}, chartOptions);
  var donutData = jQuery.extend(true, {}, data);

  // config sizing
  donutOptions.responsive = true;
  donutOptions.maintainAspectRatio = false;

  // remove ticks
  donutOptions.scales.yAxes[0].ticks = { display: false };
  donutOptions.scales.xAxes[0].ticks = { display: false };
  donutOptions.scales.xAxes[0] = { display: false };
  donutOptions.scales.yAxes[0] = { display: false };

  setChartColors("donut", donutData, color);

  // remove loader
  $(chart).parent().find(".loader").remove();

  // plot chart
  var donutChart = new Chart($("canvas", chart), {
    type: 'doughnut',
    data: donutData,
    options: donutOptions
  });

  // create legend
  if (showLegend){
    $(chart).parent().find(".legend").html(donutChart.generateLegend()).show();
  }
}
