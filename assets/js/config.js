$(function () {

  var page = $("body").attr("id");

  switch (page) {

    case "pg-config-welcome":
      initWelcomeAnimation();
      break;

    case "pg-config-dashboard":
      getAccountGrowth();
      break;

    case "pg-config-accounts":
      initConfigEditor();
      initTable(".config-accounts table", false, true, true);
      break;

    case "pg-config-users":
      initConfigEditor();
      initTable(".config-users table", false, true, true);
      break;

    // override form template redirects
    case "pg-config-billing":
    case "pg-config-email":
      $("form.use-ajax").removeAttr("data-redirect");
      break;

  }

  initNav();
  $("body").on("click", ".btn-add-stripe-plan", addStripePlan);
  $("body").on("click", ".btn-remove-stripe-plan", removeStripePlan);

});

function loadForm(val) {
  var pId = $('#selectProvider').val();
  console.log('==== 41 ====', pId)
  var paths = $(location).attr('pathname').split('/');

  var sheetId = val;
  if (!sheetId) {
    sheetId = paths.length > 2 ? paths[3] : '';
  }
  if (!sheetId || sheetId == '' || !pId || pId == '') {
    alert('Select Form or provider')
    return;
  }
  $(location).attr('href', '/report/' + pId);
}

function selectOnChange() {
  var pId = $('#selectProvider').val();
  console.log('===== 57 ====', pId)

  if (!pId || pId == '') {
    alert('Select your provider');
    return;
  }
  $(location).attr('href', '/report/' + pId);
}

function initWelcomeAnimation() {

  $(".welcome-message h1, .logo").removeClass("dim");

  setTimeout(function () {

    $(".welcome-message .btn").removeClass("dim");

  }, 1000);

}

function addStripePlan(e) {

  e.preventDefault();

  var totalPlans = $(".plan").length;
  var html = $("#plan-template").html();

  $(this).before(html);

  $(this).prev().find(".textbox").each(function () {

    var label = $(this).prev();
    label.text(label.text().replace("$", totalPlans + 1));

    var input = $(this);
    input.attr("name", input.attr("name").replace("$", totalPlans));

  });
}

function removeStripePlan(e) {

  e.preventDefault();
  $(this).parent().remove();

}

function initConfigEditor() {

  // account handlers
  $(document).on("click", ".btn-close-account", function () {

    var tr = $(this).closest("tr");
    showModal("mdl-close-account", tr.find(".email").text(), tr.attr("data-id"));

  });

  $(document).on("click", ".btn-edit-account", function () {

    var tr = $(this).closest("tr");
    var plan = tr.find(".plan .value").text();

    showModal("mdl-edit-account", null, tr.attr("data-id"));

    $(".modal input[name=email]").val(tr.find(".email").text());
    $(".modal select[name=active]").val(tr.find(".active .badge").data("active"));

    if (plan) {
      $(".modal .plan").removeClass("hide").find("select[name='plan']").val(plan);
    }
    else {
      $(".modal .plan").addClass("hide");
    }
  });

  $(document).on("change", ".modal select[name=plan]", function () {

    showSelectWarning($(this), "Customer will be charged for the new plan");

  });

  $(document).on("change", ".modal select[name=active]", function () {

    if (!parseInt($(this).val())) {
      showSelectWarning($(this), "Customer will still have account but won't be able to access it");
    }
    else {
      hideSelectWarning($(this));
    }
  });

  // user handlers
  $(document).on("click", ".btn-delete-user", function () {

    var tr = $(this).closest("tr");
    showModal("mdl-delete-user", tr.find(".email .value").text(), tr.attr("data-id"));

  });

  $(document).on("click", ".btn-edit-user", function () {

    var tr = $(this).closest("tr");

    showModal("mdl-edit-user", null, tr.attr("data-id"));

    $(".modal input[name=email]").val(tr.find(".email .value").text());
    $(".modal input[name=name]").val(tr.find(".name .value").text());
    $(".modal select[name=permission]").val(tr.find(".permission .value").text())

  });
}


function updateAccountsTable(data) {

  var row = $(".config-accounts tr[data-id='" + data.id + "']");
  row.find(".email").text(data.email);
  row.find(".plan .value").text(data.plan);

  if (data.active) {
    row.find(".active .badge").addClass("green").removeClass("red").text("active");
  }
  else {
    row.find(".active .badge").addClass("red").removeClass("green").text("cancelled");
  }
}

function updateUserTable(data) {

  var row = $(".config-users tr[data-id='" + data.id + "']");
  row.find(".email .value").text(data.email);
  row.find(".name .value").text(data.name);
  row.find(".permission .value").text(data.permission);

}

$(document).ready(function () {
  var paths = $(location).attr('pathname').split('/');
  $("#selectProvider").select2({
    ajax: {
      url: "/providers",
      dataType: 'json',
      delay: 250,
      data: function (params) {
        return {
          search: params.term, // search term
          page: params.page
        };
      },
      processResults: function (data, params) {
        // parse the results into the format expected by Select2
        // since we are using custom formatting functions we do not need to
        // alter the remote JSON data, except to indicate that infinite
        // scrolling can be used
        params.page = params.page || 1;
        return {
          results: data.results,
          pagination: {
            more: (params.page * 30) < data.total_count
          }
        };
      },
      cache: true
    },
    placeholder: 'Search by Hospital or Provider Number',
    minimumInputLength: 3
  });
  $(".table-row").click(function () {
    window.document.location = $(this).data("href");
  });
});