var stripe, card, elements;

/*
* initAccount()
* set override event handlers for account forms
*/

function initAccount(){

  $(document).on("submit", ".account.updatecard", updateCard);
  $(document).on("click", ".btn-close-account", function(e){

    showModal("mdl-close-account", null, null);

  });
};

/*
* initStripe()
* initialise stripe for updating the subscription plan or
* credit card details for the account
*/

function initStripe(){

  stripe = Stripe("pk_8mfEPykRfmyvUtWap49bUMsQ5pyHs");

  elements = stripe.elements({
    fonts: [{
      cssSrc: "https://fonts.googleapis.com/css?family=Source+Sans+Pro",
    }]
  });

  card = elements.create("card", {
    style: {
      base: {
        fontSize: '1em',
      }
    }
  });

  card.mount(".creditcard");

  // handle card errors
  card.addEventListener('change', function(event) {

    if (event.error){
      showInlineFormError($(".creditcard"), event.error.message);
    } else {
      hideInlineFormError($(".creditcard"));
    }

  });
};

/*
* initAccountUsers()
* initialise the account users page and set the modal event handlers
*/

function initAccountUsers(){

  // update invite user modal button text if multiple emails entered
  $(document).on("keydown", ".add-user-email", function(e){

    if ($(this).val().indexOf(',') > -1){
      $(".btn-confirm-add-user").val("Invite Users");
    }
    else {
      $(".btn-confirm-add-user").val("Invite User");
    }
  });

  // show add user modal
  $(document).on("click", ".btn-add-user", function(){

    showModal("mdl-add-user", null, null);

  });

  // populate and show edit user modal
  $(document).on("click", ".btn-edit-user", function(){

    var tr = $(this).closest("tr");

    showModal("mdl-edit-user", null, tr.attr("data-id"));
    $("input[name=name]").val(tr.find(".user-name").text());
    $("input[name=email]").val(tr.find(".user-email").text());
    $("select[name=permission]").val(tr.find(".user-permission").text());

  });

  // populate and show delete user modal
  $(document).on("click", ".btn-delete-user", function(){

    var tr = $(this).closest("tr");
    showModal("mdl-delete-user", tr.find(".user-email").text(), tr.attr("data-id"));

  });

  // enable table sorting
  initTable(".users-table", false, false, true);

}

/*
* updateCard()
* update the stripe credit card for the account
* overrides the standard AJAX form submit
*/

function updateCard(e){

  e.preventDefault();
  var form = $(this).closest("form");

  toggleLoadingButton("form input[type=submit]");

  stripe.createSource(card).then(function(result){

    if (result.error) {

      toggleLoadingButton("form input[type=submit]");
      showInlineFormError($(".creditcard"), result.error.message);

    }
    else {

      // inject values into hidden inputs
      $("input[name='source']").val(result.source.id);
      toggleLoadingButton("form input[type=submit]");
      submitAJAXForm(form);

    }
  });
};
