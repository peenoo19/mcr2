$(function(){

  // automatically validate all forms with the class .use-ajax
  $(document).on("submit", "form.use-ajax", function(e){

    e.preventDefault();
    submitAJAXForm($(this));

  });
})

/*
* validateForm()
* automatically validates based on the input types
* ensure you specify a type attribute and
* required if you want to force a required value
* set novalidate on the form to disable browser validation
* form: the form object
*/

function validateForm(form){

  var results = [];

  // for each input
  $("input, select", form).not(":input[type=button], :input[type=submit]")
  .each(function(){

    var input = $(this);

    switch(input.attr("type")){

      case "text":
      case "password":
      results.push(isTextValid(input));
      break;

      case "number":
      results.push(isNumberValid(input));
      break;

      case "email":
      results.push(isEmailValid(input));
      break;

      case "radio":
      results.push(isRadioValid(input));
      break;

      case "checkbox":
      results.push(isCheckboxValid(input));
      break;

      case "url":
      results.push(isURLValid(input));
      break;

      case "phone":
      results.push(isPhoneNumberValid(input));
      break;

    }

    // check for select
    if (input.is("select")){
      results.push(isSelectValid(input));
    }
  });

  // if there's more than one input
  if (results.length > 1){

    // check they're are all valid
    var valid = results.reduce(function(flag, value){
      return flag && results[0] === value;
    }, true);

  }
  else {

    // for one input, check it isn't false
    if (results[0] === false){
      valid = false;
    }
    else {
      valid = true;
    }
  }

  // send the form
  if (valid){
    return true;
  }
  else {
    return false;
  }
}

/*
* submitAJAXForm()
* handler for submitting a form via AJAX for server-side processing
* uses the form method and action
* add a data-redirect attribute if you want the form to redirect after submission
* pass a callback funtion name to res.callback and params to res.callbackParams
*/

function submitAJAXForm(form){

  toggleLoadingButton("form input[type=submit]");

  if (!validateForm(form)){
    toggleLoadingButton("form input[type=submit]");
    return false;
  }

  $.ajax({

    type: form.attr("method"),
    headers: {"Authorization": "Bearer " + $.cookie("authToken")},
    url: form.attr("action"),
    data: form.serialize(),
    success: function(res){

      toggleLoadingButton("form input[type=submit]");

      if (modalVisible){
        hideModal();
      }

      // save token?
      if (res.token){
        $.cookie("authToken", res.token, { path: '/', expires: 1 });
      }

      // redirect?
      if (form.attr("data-redirect")){
        window.location = form.attr("data-redirect");
      }

      // show notification?
      if (res.message){
        showNotification(res.message, "success", true);
      }

      // execute callback?
      if (res.callback){
        window[res.callback](res.callbackParams);
      }
    },
    error: function(res, textStatus, errorThrown){

      toggleLoadingButton("form input[type=submit]");

      if (modalVisible){
        hideModal();
      }

      // show error
      if (res.responseJSON.inputError){
        showInlineFormError($("input[name=" + res.responseJSON.inputError + "]"), res.responseJSON.message, false);
      }
      else {
        showNotification(res.responseJSON.message, "error", false);
      }
    }
  });
}

/*
* isTextValid()
* determines if a text type input is valid
*/

function isTextValid(input){

  hideInlineFormError(input);

  input.on("blur", function(){
    isTextValid(input);
  });

  return isInputBlank(input);

}

/*
* isEmailValid()
* check the the email address contains
* a valid email format - also validates
* a comma-seperarated list of emails
*/

function isEmailValid(input){

  input.on("blur", function(){
    isEmailValid(input);
  });

  if (!isInputBlank(input)){
    return false;
  };

  // check for valid email format
  if (input.val() !== ""){

    var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/

    if (re.test(input.val())){

      // email is valid
      hideInlineFormError(input);
      return true;

    }
    else {

      // email is invalid
      showInlineFormError(input, "Please enter a valid email address");
      input.on("blur", function(){ isEmailValid(input) });
      return false;

    }
  }
}

/*
* isNameValid()
* check for a full name with at least
* one letter and no numerials
*/

function isNameValid(input){

  input.on("blur", function(){
    isNameValid(input);
  });

  if (!isInputBlank(input)){
    return false;
  };

  if (input.val() !== ""){

    // check for a valid name
    if (/^[a-zA-Z'-]+$/.test(input.val())){

      hideInlineFormError(input);
      return true;

    }
    else {

      showInlineFormError(input, "Please enter your real name");
      return false;

    }
  }
}

/*
* isInputBlank()
* check string is not blank
* errorMessage: string containing a custom error
* message to be displayed on the input
*/

function isInputBlank(input, errorMessage){

  // if no error message is specified
  if (!errorMessage){

    // check for an error message on the input
    if (input.attr("data-error")){

      errorMessage = input.attr("data-error");

    }
    else {

      // use a default error message
      switch(input.attr("type")){

        case "text":
        errorMessage = "Please enter a value";
        break;

        case "password":
        errorMessage = "Please enter a password";
        break;

        case "email":
        errorMessage = "Please enter an email address";
        break;

        case "phone":
        errorMessage = "Please enter a phone number";
        break;

        case "url":
        errorMessage = "Please enter a website URL";
        break;

        default:
        errorMessage = "Please enter a value";
        break;

      }
    }
  }

  // if value is required & is blank, show error message
  if (input.prop("required") && input.val() === ""){

    showInlineFormError(input, errorMessage);
    return false;

  }

  // if value isn't required & is blank, clear the error
  if (!input.prop("required") && input.val() === ""){

    hideInlineFormError(input);
    return true;

  }

  if (input !== ""){

    hideInlineFormError(input);
    return true;

  }
}

/*
* isNumberValid()
* check the value is a number
* uses min and max attributes on the
* input to  validate the range
*/

function isNumberValid(input){

  var number = $(input).val();
  var min = $(input).attr("min");
  var max = $(input).attr("max");

  input.on("blur", function(){
    isNumberValid(input);
  });

  if (!isInputBlank(input)){
    return false;
  };

  if (isNaN(number)){

    showInlineFormError(input, "Please enter a valid number");
    return false;

  }
  else {

    if (min !== undefined && max !== undefined){

      if (number >= min && number <= max){

        return true;

      } else {

        showInlineFormError(input, "Please enter a number between " + min + " and " + max);
        return false;

      }
    }
    else {
      return true;
    }
  }
}

/*
* isCheckboxValid()
* check the minimum number of checkboxes has been selected
* use data-min and data-max on the input to specify the range
*/

function isCheckboxValid(checkbox){

  var fieldset = checkbox.closest("fieldset");
  var errorMessage, min, max;

  // reset the fieldset
  $(fieldset).removeClass("error").find(".error-message").remove();

  // re-validate the checkbox on change
  $(".checkbox input", fieldset).on("change", function(){
    isCheckboxValid(fieldset, min);
  });

  // get the min and max values
  if (fieldset.attr("data-min")){
    min = fieldset.attr("data-min");
  }

  if (fieldset.attr("data-max")){
    max = fieldset.attr("data-max");
  };

  // test and show errors
  if ($(".checkbox input:checked", fieldset).length < min){

    $(fieldset).addClass("error")
    .append('<div class="error-message">Please select at least ' + min + ' option</div>');
    return false;

  }
  else if ($(".checkbox input:checked", fieldset).length > max){

    $(fieldset).addClass("error")
    .append('<div class="error-message">Maximum allowed is ' + max + '</div>');
    return false;

  }
  else {

    $(fieldset).removeClass("error").find(".error-message").remove();
    return true;

  }
}

/*
* isRadioValid()
* check that at least one radio has been selected
*/

function isRadioValid(radio){

  var fieldset = radio.closest("fieldset");
  var errorMessage;

  // reset the fieldset
  $(fieldset).removeClass("error").find(".error-message").remove();

  // re-validate the radio on change
  $(".radio input", fieldset).on("change", function(){
    isRadioValid(fieldset);
  });

  if (fieldset.attr("data-required")){

    if ($(".radio input:checked", fieldset).length < 1 ){

      $(fieldset).addClass("error")
      .append('<div class="error-message">Please select at least one value</div>');
      return false;

    }
    else {

      $(fieldset).removeClass("error").find(".error-message").remove();
      return true;

    }
  }
}

/*
* isURlValid()
* checks that an input contains a valid URL
*/

function isURLValid(input){

  input.on("blur", function(){
    isURLValid(input);
  });

  if (!isInputBlank(input)){
    return false;
  };

  // if value is present, check that it's valid
  if (input.val() !== ""){

    // check for valid url format
    if (/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(input.val())){

      // url is valid
      hideInlineFormError(input);
      return true;

    }
    else {

      // url is invalid
      showInlineFormError(input, "Please enter a valid URL");
      input.on("blur", function(){ isURLValid(input); });
      return false;

    }
  }
}

/*
* isPhoneNumberValid()
* checks if an input contains a valid phone number
* this is a loose test to check for numbers, spaces or brackets ()
* and the regex will need to be tailored to test for
* specific country code formats
*/

function isPhoneNumberValid(input){

  input.on("blur", function(){
    isPhoneNumberValid(input);
  });

  if (!isInputBlank(input)){
    return false;
  };

  if (input.val() !== ""){

    // check for valid phone number format
    if (/^(?=.*[0-9])[- +()0-9]+$/.test(input.val())){

      // phone number is valid
      hideInlineFormError(input);
      return true;

    }
    else {

      // phone number is invalid
      showInlineFormError(input, "Please enter a valid phone number");
      input.on("blur", function(){ isPhoneNumberValid(input); });
      return false;

    }
  }
}

/*
* checkStringLength()
* check the a string is longer or shorter than the specified range
* input: jQuery object of the input
* len: desired length of string
* range: "min" or "max" to determine if string is longer or shorter
* than len
* displayed on the input
*/

function checkStringLength(input, len, range, errorMessage){

  if (range === "min"){

    if ($(input).val().length > len){

      hideInlineFormError(input);
      return true;

    }
    else {

      showInlineFormError(input, errorMessage);
      return false;

    }
  }
  else if (range === "max"){

    if ($(input).val().length < len){

      showInlineFormError(input, errorMessage);
      return false;

    }
    else {

      hideInlineFormError(input);
      return true;

    }
  }
}

/*
* isSelectValid()
* ensures that an option has been selected in a select dropdown
*/

function isSelectValid(select){

  $(select).on("change", function(){
    isSelectValid(select);
  });

  if (select.prop("required") && select.val().toLowerCase() === "unselected"){

    showSelectError(select);
    return false;

  }
  else {

    hideSelectError(select);
    return true;

  }
}

/*
* showInlineFormError()
* show an error message on the specified input
*/

function showInlineFormError(input, errorMessage){

  hideInlineFormError(input);
  input.addClass("error").after("<div class='error-message'>" + errorMessage + "</div>");

}


/*
* hideInlineFormError()
* remove an error on the specified input
*/

function hideInlineFormError(input){

  $(input).removeClass("error");

  if ($(input).next().hasClass("error-message")){
    $(input).next().remove();
  }
}

/*
* showSelectError()
* show an error message on the specified select input
*/

function showSelectError(select){

  hideSelectError(select);
  $(select.parent()).addClass("error").append("<div class='error-message'>Please select an option</div>");

}

/*
* hideSelectError()
* removes an error message on the specified select
*/

function hideSelectError(select){

  $(select.parent()).removeClass("error");
  $(".error-message", select.parent()).remove();

}


/*
* hideSelectWaring()
* show a warning message on the specified select input
*/

function showSelectWarning(select, warningMessage){

  hideSelectWarning(select);
  $(select.parent()).addClass("warning").append("<div class='warning-message'>" + warningMessage + "</div>");

}

/*
* hideSelectWarning()
* removes a warning message on the specified select
*/

function hideSelectWarning(select){

  $(select.parent()).removeClass("warning");
  $(".warning-message", select.parent()).remove();

}

/*
* doesHashExist()
* check if a hash value is present in the string
* hash: string to be tested, eg. url string
* errorMessage: string containing error message to
* be displayed in notification
*/

function doesHashExist(hash, errorMessage){

  if (hash.length > 0){
    return true;
  }
  else {
    showNotification(errorMessage, "error", false);
  }
}
