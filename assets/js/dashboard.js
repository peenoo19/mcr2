/*
* this is for demo purposes only
* you can remove all the code in this file
* and replace it with your own dashboard logic
*/

var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

var label = "Revenue";

var revenue = {

  labels: months,
  datasets: [{

    label: label,
    data: [64232, 64647, 56338, 55347, 54462, 62374, 66334, 69573, 71464, 69464, 75474, 78757]

  }]
}

var goals = [75, 96, 49, 24]

function initDashboard(){

  createLineChart(".revenue", revenue, [blueChart], false);

  $(".goal .progress-bar").each(function(index){
    updateProgressBar($(this), goals[index], true);
  })

  initTable(".user-table", false, false, true);

  // user table functions
  $(document).on("click", ".btn-edit-user", function(){

    var tr = $(this).closest("tr");

    showModal("mdl-edit-user", null, tr.attr("data-id"));
    $("input[name=name]").val(tr.find(".user-name").text());
    $("input[name=email]").val(tr.find(".user-email").text());
    $("select[name=plan]").val(tr.find(".user-plan").text());

  });

  $(document).on("submit", ".edit-user", function(){

    var tr = $(".user-table tr[data-id=" + $(this).find(".id").val() + "]");
    tr.find(".user-name").text($("input[name=name]").val());
    tr.find(".user-email").text($("input[name=email]").val());
    tr.find(".user-plan .badge").html($("select[name=plan]").val());

  });

  $(document).on("click", ".btn-delete-user", function(){

    $(this).closest("tr").remove();

  });
}
