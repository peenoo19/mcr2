/*
* call the initializer functions for each page
*/

$(function(){
  console.log('---------- ')
  var page = $("body").attr('class').split(" ")[0];

  switch (page){

    case "pg-signup-account":
    initAuth();
    initSignupAccount();
    initStripe();
    break;

    case "pg-signup-user":
    initAuth();
    break;

    case "pg-accountprofile":
    initAccount();
    break;

    case "pg-accountbilling":
    initTabs();
    initAccount();
    initStripe();
    break;

    case "pg-accountusers":
    initNav();
    initAccountUsers();
    break;

    case "pg-dashboard":
    initDashboard();
    break;

    case "pg-account":
    initTabs();
    break;

  }
});
