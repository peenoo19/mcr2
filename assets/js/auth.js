/*
* initAuth()
* set override handlers for authentication forms
*/

function initAuth(){

  $(document).on("submit", ".auth.signup-account", signupAccount);
  $(document).on("submit", ".auth.signup-user", signupUser);

}

/*
* initSignupAccount()
* set the selected plan
*/

function initSignupAccount(){

  // set the selected plan
  var plan = location.hash.substring(1);

  if (plan != ""){
    $("select[name='plan']").val(plan);
  }
}

/*
* signupAccount()
* sign up a new account and process the payment with stripe
* over rides the default form submit
*/

function signupAccount(e){

  e.preventDefault();
  var form = $(this).closest("form");

  toggleLoadingButton("form input[type=submit]");
  validateForm(form);

  // validate stripe card input
  stripe.createSource(card).then(function(result){

   if (result.error){

     toggleLoadingButton("form input[type=submit]");
     showInlineFormError($(".card-element"), result.error.message);

   }
   else {

     // inject values into hidden inputs
     $("input[name='source']").val(result.source.id);
     $("input[name='referrer']").val(document.referrer);

     // submit the form
     toggleLoadingButton("form input[type=submit]");
     submitAJAXForm(form);

   }
  });
}

/*
* signupUser()
* sign up a new user from an account invitation
*/

function signupUser(e){

  e.preventDefault();
  var form = $(this).closest("form");

  // inject the inviteId
  $("input[name='inviteId']").val(window.location.hash.substring(1));
  submitAJAXForm(form);

}
