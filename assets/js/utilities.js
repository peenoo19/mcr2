/*
* isMobile()
* determine if the user is using a mobile device
* returns true or false
*/

function isMobile(){

  if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    return true;
  }
  else {
    return false;
  }
}

/*
* formatToMetric()
* format the specified number to a metric value
* 10,000 becomes 10k, 1,000,000 becomes 1m
* num: the number to be formatted
* currency: (optional) prepend a currency symbol
* decimal: (optional) specify the number of decimal places
*/

function formatToMetric(num, currency, decimal){

  if (!decimal){
    decimal = 0;
  }

  if (!currency){
    curreny = "";
  }

  if (num > 999 && num < 1000000){
    return currency + (num/1000).toFixed(decimal) + "k";
  }
  else if (num >= 1000000){
    return currency + (num/1000000).toFixed(decimal) + "m";
  }
  else if (num < 0 && num < -999){
    return "-" + currency + Math.abs((num/1000).toFixed(decimal)) + "k";
  }
  else if (num < -1000000){
    return "-" + currency + Math.abs((num/1000000)).toFixed(decimal) + "m";
  }
  else {
    return num;
  }
}

/*
* formatWithCommas()
* formats number with commas
* eg. 123456789 becomes 123,456,789
* value: the number to be formatted
* currency: (optional) currency symbol to be prepended
*/

function formatWithCommas(value, currency){

  if (currency){

    if (value < 0){
      return "-" + currency + Math.abs(value).toLocaleString();
    }
    else {
      return currency + value.toLocaleString();
    }
  }
  else {
    return value.toLocaleString();
  }
}
