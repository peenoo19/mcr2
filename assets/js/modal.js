var modalVisible = false;

/*
* hideModal()
* hide and reset the current modal
*/

function hideModal(){

  modalVisible = false;

  $(".modal").fadeTo(300, 0).find(".modal-content")
  .animate({ top: "0%", opacity: "0" }, 300 , function(){

    $(".modal").hide().empty();
    $(".modal-content").remove();
    $(".custom").text("");

  });
}

/*
* showModal()
* show an overlay modal in the center of the screen
* modalName: contains the id of the script template containing the modal HTML
* custom: (optional) custom string to be injected into the modal copy
* id: (optional) data-attribute to be added to a hidden field in the modal
*/

function showModal(modalName, custom, id){

  modalVisible = true;

  // clear the previous modal
  $('.modal').empty();
  $(".modal").append("<div class='modal-content card'></div>");
  $(".modal-content").html($("#" + modalName ).html());

  // set id and custom text
  if (id){
    $(".modal-content .id").val(id);
  }

  if (custom){
    $(".modal-content .custom").text(custom);
  }

  // show the modal
  $(".modal").show().fadeTo(300, 1).find(".modal-content")
  .animate({ top: "50%", opacity: "1" }, 300 );

  // bind cancel model button handler
  $(".modal .btn-cancel").on("click", function(e){
    e.preventDefault();
    hideModal();
  });

  // close modal when clicking outside of it
  $(".modal").on("click", function(e) {

    e.stopPropagation();
    hideModal();

  }).children().on("click", function(e){

    e.stopPropagation();
    if ($(this).hasClass('button')) {
      return false;
    }
  });

  // close modal button handler
  $(".modal .btn-close-modal").on("click", function(){
    hideModal();
  });
}
