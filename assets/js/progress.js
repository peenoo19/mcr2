/*
* updateProgressBar()
* update the progress bar width
* bar: progress bar container class, eg. .tickets-sold
* width: percentage value: "80%"
*/

function updateProgressBar(bar, width, color){

  if (color){

    if (width > 75){
      bar.addClass("green").removeClass("red orange");
    }

    if (width < 50){
      bar.addClass("orange").removeClass("green red");
    }

    if (width < 25){
      bar.addClass("red").removeClass("gren orange");
    }
  }

  width = width + "%";
  bar.find(".progress-fill").width(width);

}

/*
* toggleLoadingButton()
* toggle the loading animation on a button
* when performing a server action
* button must be wrapped with .btn-loader
* button: jQuery object of the target button
*/

function toggleLoadingButton(button){

  if ($(button).is(':disabled')){
    $(button).prop("disabled", false);
  }
  else {
    $(button).prop("disabled", true);
  }

  $(button).parent().toggleClass("loading");

}

/*
* toggleLoadingScreen()
* toggle a fullscreen loading message and
* restrict all user actions until loading has completed
* state: true/false to show or hide the loading screen
* message: custom string to be displayed as the loading message
*/

function toggleLoadingScreen(state, message){

  var loader =
  '<div class="loader-wrapper">' +
  '<div class="loader sk-dots">' +
  '<div class="sk-child sk-bounce1"></div>' +
  '<div class="sk-child sk-bounce2"></div>' +
  '<div class="sk-child sk-bounce3"></div>' +
  '</div>' +
  '<div class="loader-message">' + message + '</div>' +
  '</div>';

  $("body").toggleClass("loading");

  if (state){
    $("html").prepend(loader);
  }
  else {
    $(".loader").remove();
  }
};
