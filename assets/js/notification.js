var notificationTimeout = 2500; // 2.5 second delay before notification disapears

/*
* hideNotification()
* hide and reset the notification
*/

function hideNotification(){

  $(".notification").text("").removeClass().addClass("notification");

}

/*
* showNotification()
* show an iOS-style notification at the top of the page
* message: string to be displayed
* type: "error" or "success"
* autoClose: true/false to automatically remove the notificatin
* after the time specified in notificationTimeout
*/

function showNotification(message, type, autoClose){

  $(".notification").text(message).addClass("show").addClass(type);

  if (autoClose){

    setTimeout(function(){ hideNotification(); }, notificationTimeout);

  }
  else {

    $(".notification").append("<button class='btn-close-notification ico ico-light ico-cross'></button>");
    $(document).on("click", ".btn-close-notification", hideNotification);

  }
}
