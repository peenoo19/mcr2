/*
* closeMessage()
* hides a message element when clicking the X button
*/

var closeMessage = function(){

  $(this).parent().addClass("hide");

};

/*
* toggleBlankSlate()
* toggle the blank slate message
* use when all content has been deleted on screen and
* you need to display a blank slate message with a call to action
*/

var toggleBlankSlate = function(state){

  if (state){
    $("main > *").addClass("hide");
    $(".blankslate-message").removeClass("hide");
  }
  else {
    $("main > *").removeClass("hide");
    $(".blankslate-message").addClass("hide");
  }
};

/*
* initMessage()
* initialise the message UI
*/

var initMessage = function(){

  $(document).on("click", ".btn-close-message", closeMessage);

};
