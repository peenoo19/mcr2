/*
* initRating()
* enable the user to set the rating by
* hovering/clicking on the rating stars
* call on any page that uses a rating input selector
*/

var initRating = function(){

  var selector, rating;

  $(document).on("mouseover", ".rating.selector li", function(){

    selector = $(this).parent();
    var eq = $(this).index()+1;

    switch (eq){

      case 1:
      selector.removeClass().addClass("rating selector one-star");
      rating = 1;
      break;

      case 2:
      selector.removeClass().addClass("rating selector two-star");
      rating = 2;
      break;

      case 3:
      selector.removeClass().addClass("rating selector three-star");
      rating = 3;
      break;

      case 4:
      selector.removeClass().addClass("rating selector four-star");
      rating = 4;
      break;

      case 5:
      selector.removeClass().addClass("rating selector five-star");
      rating = 5;
      break;

    }
  });
};
