function getAccountGrowth(){

  $.ajax({

    type: "GET",
    url: "/api/metrics/accounts/growth",
    headers: {"Authorization": "Bearer " + $.cookie("authToken")},
    success: function(res){

      createLineChart(".chart.usergrowth", res.chart, [blueChart], false);

    },
    error: function(res, textStatus, errorThrown){

      showNotification(res.responseJSON.message, "error", false);

    }
  });
}
