/*
* switchTab()
* switches the view when clicking on a tab
* target: href #id of the target view
* automatically passed when clicking on .tabs a
*/

function switchTab(target){

  // switch tab
  var tab = $("a[href$='" + target + "']");
  $(".tabs .active").removeClass("active");
  $(tab).addClass("active");

  // switch tab view
  $(".tabview").addClass("hide");
  $(target).removeClass("hide");

}

/*
* initNav()
* initialise the main nav and mobile view
* call on all app pages show the primary nav
*/

var initNav = function(){

  // toggle nav
  $(document).on("click", ".btn-togglenav", function(){

    $(this).parent().toggleClass('open');

    if ($(this).parent().hasClass("open")){
      $(this).removeClass("ico-nav").addClass("ico-cross");
    }
    else {
      $(this).removeClass("ico-cross").addClass("ico-nav");
    }
  });
};

/*
* initTabs()
* initialise the tabs UI
* call on any page that uses tabs
*/

var initTabs = function(){

  // switch to active tab on page load
  if (document.location.hash){
    switchTab(document.location.hash);
  }

  // switch tab event handler
  $(document).on("click", ".tabs a", function(e){
    e.preventDefault();
    switchTab($(this).attr("href"));
  });

};
