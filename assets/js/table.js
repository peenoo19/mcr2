/*
* initTable()
* initialise a dynamic table
* table: class or id name of table
* paging: true/false to determine if table is paged
* search: true/false to determine if table can be searched
* sort: true/false to determine of table is sortable
*/

function initTable(table, paging, search, sort){

  $(table).DataTable({

    paging: paging,
    searching: search,
    ordering: sort

  });
}

/*
* deleteTableRow()
* remove a table row from the ui
* id: data-id of the table row
*/

function deleteTableRow(trId){

  $("tr[data-id=" + trId + "]").remove();

}

/*
* updateTableRow()
* update the values in a table row
* data should contain an id and values array
* eg. data = { id: 1, values: [1,2,3] }
*/

function updateTableRow(data){

  var td = $("tr[data-id=" + data.id + "] td");
  var i = 0;

  td.each(function(){

    if (data.values[i] !== null){
      $(this).html(data.values[i]);
    }

    i++;

  });
}

/*
* addTableRow()
* appends a new table row to the ui
* table: class or id name of the table
* templateID: #id of the script template containing
* the table row html
*/

function addTableRow(table, templateID){

  var tr = $("#" + templateID).html();
  $("tbody", table).append(tr);

}
