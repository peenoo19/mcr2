*********************
Welcome to Gravity 🚀
*********************

Thank you for purchasing Gravity, this readme will help you get set up and
running in the next few minutes.


# Requirements

Before you can use the full application without errors, you will need to register
and set up a Stripe (https://stripe.com) and Mailgun (https://mailgun) account.

Full instructions on how to do this are provided in the documentation available
at https://MedicareCostReport.net/docs/getting-started/welcome

You will also need an empty MySQL database. For live deployments, you will
need an SSL certificate installed on your domain. However; this isn't necessary
for building and testing your application.


# Installing Gravity

If you haven't already installed Node.js on your machine, please download
and install the latest version (https://nodejs.org/en/download/)

It's also recommended that you install the node package manager (NPM)
(https://www.npmjs.com/) for easy installation of Node.js packages from
the command line.

Done? OK, now open up a new terminal window and navigate to the Gravity folder
you downloaded and run:

npm install

You will now be able to run the application to access the full documentation.

Run:

node server

In a separate tab, compile the front-end JS:

gulp compileAppJS

Then open up a new browser window and navigate to http://localhost:8080/setup
which will guide you through the process for setting up your application.

After you have completed the setup process, restart your node server and open
a new browser tab: http://localhost:8080

Failure to follow these steps, will result in your application throwing errors.

If you have any problems installing Gravity, please read the documentation:

https://MedicareCostReport.net/docs or contact us: support@MedicareCostReport.net
