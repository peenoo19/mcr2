var gulp = require("gulp");
var scss = require("gulp-sass");
var concat = require("gulp-concat");

// directories
const scssDir = "assets/scss/";
const cssOutputDir = "assets/css";
const jsDir = "assets/js/";
const jsOutputDir = "assets/js/min";

// watch
gulp.task('default', function(){

  gulp.watch(scssDir + "*.scss", gulp.series("compileAppCSS"));
  gulp.watch(scssDir + "*.scss", gulp.series("compileHomeCSS"));
  gulp.watch(jsDir + "*.js", gulp.series("compileAppJS"));

});

// compile app css
gulp.task("compileAppCSS", function(done){

  gulp.src(scssDir + "app.scss")
  .pipe(scss({ style: "expanded" }))
  .pipe(gulp.dest(cssOutputDir));
  done();

});

// compile home css
gulp.task("compileHomeCSS", function(done){

  gulp.src(scssDir + "home.scss")
  .pipe(scss({ style: "expanded" }))
  .pipe(gulp.dest(cssOutputDir));
  done();

});

// compile app.js
gulp.task("compileAppJS", function(done){

  gulp.src(jsDir + "/*")
  .pipe(concat("app.min.js"))
  .pipe(gulp.dest(jsOutputDir));
  done();

});
