const db = require("./database");
const bcrypt = require("bcrypt");
const mail = require("./mail");
const settings = require("../settings");
const randomstring = require("randomstring");

db.init();

/*
* user.create()
* register a new user and associate it with the parent account
*/

exports.create = async function(name, email, password, accountId, permission){

  // encrypt password
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);

  // add user to database
  const sql = "INSERT INTO user VALUES(null, ?, ?, ?, NOW(), NOW(), ?, ?, ?)";
  const data = await db.query(sql, [name, email, hash, permission, accountId, accountId]);
  return data.insertId;

}

/*
* user.get()
* get a user by email or user id and account id
*/

exports.get = async function(userId, email, accountId){

  let selector, params;

  if (email){
    selector = "user.email = ?";
    params = [email];
  }
  else {
    selector = "user.id = ? AND account_id = ?";
    params = [userId, accountId];
  }

  const sql = "SELECT id, name, email, date_created, last_login, " +
  "permission, account_id FROM user WHERE " + selector;

  const data = await db.query(sql, params);
  return data[0];

}

exports.getUserByID = async function(userId) {
  
  const sql = "SELECT * from user where id = ?";
  const data = await db.query(sql, [userId]);
  // console.log(data);
  return data[0];
};

exports.updateAccountId = async function(userId, account_id, accountId) {
  let accountIds;

  if (!!account_id) {
    accountIds = account_id + ',' + accountId;
  } else {
    accountIds = accountId;
  }

  const sql = "UPDATE user SET all_account_ids = ? where id = ?";
  const data = await db.query(sql, [accountIds, userId]);
  if (data) {
    return ({
      message: 'success',
      status: 200
    });
  }
};

exports.createUserPurchasePlan = async function (userId, accountId, planName, planPrice) {

  let sqldel = "";
  let params = [];
  // delete free plan if exist
  sqldel = "UPDATE userpurchaseplan SET planName = ?, price = ? WHERE userdId = ? AND planName = ?";
  paramsdel = [planName, planPrice , userId, 'FREE-PLAN-0'];
  const chk = await db.query(sqldel, paramsdel);
  console.log("CHK "+chk.changedRows)
  if(chk.changedRows != 0){
    return ({
      message: 'success',
      status: 200
    });
  }else{
    //sqldel = "DELETE FROM userpurchaseplan WHERE userdId = ? AND accountId = ? AND planName = ? AND price = ?";
    //params = [userId, accountId, 'FREE-PLAN-0', '0'];
    //await db.query(sqldel, params);
    // insert into
    const sql = "INSERT INTO userpurchaseplan values(null,?,?,?,?)";
    const data = await db.query(sql, [userId, accountId, planName, planPrice]);

    if (data) {
      return ({
        message: 'success',
        status: 200
      });
    }
  }

} 

/*
* user.password()
* return the hashed user password
*/

exports.password = async function(userId, accountId){

  const sql = "SELECT password FROM user WHERE id = ? AND account_id = ?";
  const hash = await db.query(sql, [userId, accountId]);
  return hash[0].password;

}

/*
* user.password-verify()
* check the password against the hash stored in the database
*/

exports.password.verify = async function(userId, accountId, password){

  const sql = "SELECT password FROM user WHERE id = ? AND account_id = ?";
  const hash = await db.query(sql, [userId, accountId]);
  const verified = await bcrypt.compare(password, hash[0].password);

  if (verified) return userId;
  else return false;

};

/*
* user.password.save()
* save a new password for the user
* if not executed via a password reset request, the user is notified
* by email that their password has been changed
* passwordReset: true/false to determine of password update is part of reset
*/

exports.password.save = async function(userId, password, passwordReset){

  // encrypt the password
  const salt = await bcrypt.genSalt(10);
  const hash = await bcrypt.hash(password, salt);

  // save the password
  let sql = "UPDATE user SET password = ? WHERE id = ?";
  await db.query(sql, [hash, userId]);

  // get email
  sql = "SELECT name, email FROM user WHERE id = ?";
  const data = await db.query(sql, [userId]);

  // send the email
  if (!passwordReset)
    mail.send(data[0].email, "Your password has been changed", "password-updated", { name: data[0].name });

  return({ success: true, message: "Your password has been updated" });

}

/*
* user.update()
* update the user profile
* profile: object containing the user data to be saved
* role: user permission priviledges, eg. "admin" or "user"
*/

exports.update = async function(userId, accountId, profile, permission){

  let message, sql, params;

  if (permission == "master"){

    sql = "UPDATE user SET email = ?, name = ?, permission = ? WHERE id = ?";
    params = [profile.email, profile.name, profile.permission, userId];
    message = profile.email + " has been updated";

  }
  else if (permission == "admin" || permission == "owner"){

    sql = "UPDATE user SET email = ?, name = ?, permission = ? WHERE id = ? AND account_id = ?";
    params = [profile.email, profile.name, profile.permission, userId, accountId];
    message = profile.email + " has been updated";

  }
  else {

    sql = "UPDATE user SET email = ?, name = ? WHERE id = ? AND account_id = ?";
    params = [profile.email, profile.name, userId, accountId];
    message = "Your profile has been updated"

  }

  await db.query(sql, params);

  return ({

    success: true,
    message: message,
    callback: "updateTableRow",
    callbackParams: { id: userId, values: [profile.name, profile.email, profile.permission] }

  });
}

/*
* user.update.lastlogin()
* update the last login time for the user
*/

exports.update.lastlogin = async function(userId, accountId){

  const sql = "UPDATE user SET last_login = NOW() WHERE id = ? and account_id = ?";
  db.query(sql, [userId, accountId]);

}

/*
* user.invite.send()
* return the invite for the new user
*/

exports.invite = async function(inviteId, email){

  let sql = "SELECT id, email FROM account WHERE id = " +
  "(SELECT account_id FROM invite WHERE invite_id = ? " +
  "AND email = ? AND used = 0)";

  const data = await db.query(sql, [inviteId, email]);

  if (data.length > 0){

    return {
      account_id: data[0].id,
      email: data[0].email
    }

  }
  else
    return false;

}

/*
* user.invite.create()
* create a new user invite to join an account
*/

exports.invite.create = async function(email, accountId){

  // check if user has already been invited
  const sql = "SELECT account_id, invite_id FROM invite WHERE email = ? AND used = 0";
  const invite = await db.query(sql, [email]);

  // user has been invited, re-send invite
  if (invite.length){

    const inviteURL = settings.domain + "/signup/user#" + invite[0].invite_id;
    const sql = "UPDATE invite SET date_sent = NOW()";
    await db.query(sql);
    return inviteURL;

  }
  else {

    // create a new invite
    const inviteID = randomstring.generate(16);
    const inviteURL = settings.domain + "/signup/user#" + inviteID;
    const sql = "INSERT INTO invite VALUES (null, ?, ?, ?, NOW(), false)";
    await db.query(sql, [inviteID, email, accountId]);
    return inviteURL;

  }
}

/*
* user.invite.close()
* makes the invite as used so it can't be used again
*/

exports.invite.close = async function(inviteId){

  // set invite status to used so it can't be used again
  sql = "UPDATE invite SET used = true WHERE invite_id = ?";
  db.query(sql, [inviteId]);

}

/*
* user.delete()
* delete the user
*/

exports.delete = async function(userId, accountId, permission){

  let sql = "";
  let params = [];

  if (permission == "master"){

    sql = "DELETE FROM user WHERE id = ?";
    params = [userId];

  }
  else {

    sql = "DELETE FROM user WHERE id = ? AND account_id = ?";
    params = [userId, accountId];

  }

  await db.query(sql, params);

  return ({

    success: true,
    message: "User removed",
    callback: "deleteTableRow",
    callbackParams: userId

  });
};
