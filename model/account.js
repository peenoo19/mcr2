const db = require("./database");

db.init();

/*
* account.create()
* create a new account and return the account id
*/

exports.create = async function(account, stripe){

  let sqldel = "";
  let paramsdel = [];
  // delete free plan if exist
  //const sql = "UPDATE account SET plan = ? WHERE id = ?";
  //db.query(sql, [plan, accountId]);


  sqldel = "UPDATE account SET plan = ? ,stripe_customer_id = ?, stripe_subscription_id = ? , referrer = ?, active = ? WHERE email = ? AND plan = ?";
  paramsdel = [account.plan, stripe.customer.id, stripe.subscription.id, account.referrer, 1, account.email, 'FREE-PLAN-0'];
  const chk = await db.query(sqldel, paramsdel);
  console.log("CHK "+chk.changedRows)
  if(chk.changedRows != 0){
    return 0;
  }else{
    const sql = "INSERT INTO account (email, stripe_customer_id, " +
        "stripe_subscription_id, plan, referrer, active) VALUES(?, ?, ?, ?, ?, ?)";

    const params = [account.email, stripe.customer.id,
      stripe.subscription.id, account.plan, account.referrer, 1];

    const data = await db.query(sql, params);
    return data.insertId;
  }


}

/*
* account.get()
* get an account by email or id
*/

exports.get = async function(accountId, email){

  let selector, params;

  if (email){
    selector = "account.email = ?";
    params = [email];
  }
  else {
    selector = "account.id = ?";
    params = [accountId];
  }

  let sql = "SELECT account.id, account.email, account.date_created, " +
  "stripe_customer_id, stripe_subscription_id, plan, active, user.name " +
  "FROM account, user WHERE user.account_id = account.id AND " + selector;

  let data = await db.query(sql, [params]);
  return data[0];

}

exports.getUserPlans = async function(email){

  let plans = [];

  if (email){
    let sql = 'SELECT plan FROM account WHERE account.email = (?)';
    let data = await db.query(sql, [email]);
    for (i = 0; i < data.length; i++){
      plans.push({
        plan: data[i].plan
      });
    }

  }
  return plans;

}

/* Fetch all accounts of provided id's */
exports.getAccountByIds = async function (accountIds) {
  // let selector = '(';
  // let param;

  for (let index = 0; index < accountIds.length; index++) {
    accountIds[index] = parseInt(accountIds[index]);
  }

  const sql = 'SELECT date_created,plan FROM account where id in (?)';
  let data = await db.query(sql, [accountIds]);
  return data;
};

/*
* account.update()
* update the account profile
*/

exports.update = async function(accountId, profile){

  const sql = "UPDATE account SET email = ? WHERE id = ?";
  await db.query(sql, [profile.email, accountId]);
  return true;

}

/*
* account.update.plan()
* update the account subscription plan
*/

exports.update.plan = async function(accountId, plan){

  const sql = "UPDATE account SET plan = ? WHERE id = ?";
  db.query(sql, [plan, accountId]);

}

/*
* account.users()
* returns the users belonging to an account
*/

exports.users = async function(accountId){

  let users = [];

  const sql = "SELECT id, email, name, permission " +
  "FROM user WHERE user.account_id = ?";

  const data = await db.query(sql, [accountId]);

  // put results into arr
  for (i = 0; i < data.length; i++){
    users.push({

      user_id: data[i].id, email: data[i].email,
      name: data[i].name, permission: data[i].permission

    });
  }

  return users;

};

/*
* account.delete()
* delete the account and all its users
*/

exports.delete = async function(accountId){

  const sql = "DELETE FROM account WHERE id = ?";
  await db.query(sql, [accountId]);
  return true;

};

/*
* account.activate()
* set the activation status of an account
*/

exports.activate = async function(accountId, state){

  const sql = "UPDATE account SET active = ? WHERE id = ?";
  db.query(sql, [state, accountId]);

}
