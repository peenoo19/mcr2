const settings = require("../settings");

exports.convertToMonthName = function(month){

  const monthNames = ["Jan", "Feb", "Mar", "Apr", "May",
  "Jun", "Jul","Aug", "Sep", "Oct", "Nov", "Dec"]

  return monthNames[month-1];

}

exports.getBillingPlanDetails = function(planId){

  let planObj = settings.stripe.plans.find(planObj => planObj.id == planId);
  return planObj;

}
