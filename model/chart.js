/*
* chart objects for storing chart labels, datasets and labels
* object is then passed to front-end via ajax to plot chart
* .addDataset method pushes a new dataset to the chart object
*/

exports.line = function(labels, dataset, label){

  this.datasets = [];
  this.labels = [];

  // labels & dataset can be added at construction or pushed later
  if (labels)
    this.labels = labels

  if (dataset)
    this.datasets.push(new linechartDataset(dataset, label));

}

exports.line.prototype.addDataset = function(data, label){

  this.datasets.push(new linechartDataset(data, label));

}

function linechartDataset(data, label){

  this.label = label,
  this.data = data

}

/* sparkline */
exports.sparkline = function(dataset, label){

  this.datasets = [];

  if (dataset)
    this.datasets.push(new sparklineDataset(dataset, label));

}

function sparklineDataset(data, label){

  this.label = label,
  this.data = data

}

/* barchart */
exports.bar = function(labels, dataset, label){

  this.datasets = [];
  this.labels = [];

  // labels & dataset can be added at construction or pushed later
  if (labels)
    this.labels = labels

  if (dataset)
    this.datasets.push(new barchartDataset(dataset, label));

}

function barchartDataset(data, label){

  this.label = label,
  this.data = data

}

exports.bar.prototype.addDataset = function(data, label){

  this.datasets.push(new barchartDataset(data, label));

}

/* piechart */
exports.pie = function(labels, dataset, label){

  this.datasets = [];
  this.labels = [];

  if (labels)
    this.labels = labels

  if (dataset)
    this.datasets.push(new piechartDataset(dataset, label));

}

function piechartDataset(data, label){

  this.label = label,
  this.data = data

}
