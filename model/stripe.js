const settings = require("../settings");
const db = require("./database");
const stripe = require("stripe")(settings.stripe.secretAPIKey);

db.init();

/*
* stripe.subscription()
* return a stripe subscription
*/

exports.subscription = async function(subscriptionId){

  if(subscriptionId.length == 0){
    return false;
  }else{
    return await stripe.subscriptions.retrieve(subscriptionId);
  }


}

/*
* stripe.subscription.update
* upgrade or downgrade the stripe subscription to a different plan
*/

exports.subscription.update = async function(subscription, plan){

  let updatedSubscription = await stripe.subscriptions.update(subscription.id, {
    items: [{ id: subscription.items.data[0].id, plan: plan }]
  });

  return updatedSubscription;

}

/*
* stripe.customer()
* return a stripe customer
*/

exports.customer = async function(customerId){

  return stripe.customers.retrieve(customerId);

}

/*
* stripe.customer.create()
* create a new stripe customer
* source: passed from front-end payment form
*/

exports.customer.create = async function(email, source){

  const customer = await stripe.customers.create({
    email: email,
    source: source
  });

  return customer;

};


/* stripe.customer.update(){
* update the customers card details
* source: passed from the front-end
*/

exports.customer.update = async function(customerId, source){

  const customer = await stripe.customers.update(customerId, {
    source: source
  });

  return true;
}

/*
* stripe.customer.subscribe()
* subscribe the stripe customer to a plan
*/

exports.customer.subscribe = async function(customerId, plan){

  let subscription = await stripe.subscriptions.create({

    customer: customerId,
    items: [{ plan: plan }]

  });

  // add the price
  subscription.price = settings.stripe.currencySymbol +
  (subscription.items.data[0].plan.amount / 100).toFixed(2);

  subscription.discount = settings.stripe.currencySymbol + 12.00;

  console.log('AMan', subscription.items.data[0].plan.amount);
  console.log('AMan', subscription.discount);

  return subscription;

}

/*
* stripe.customer.delete()
* deletes a stripe customer
*/

exports.customer.delete = async function(customerId){

  await stripe.customers.del(customerId);
  return true;

};
