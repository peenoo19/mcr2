const db = require("./database");

db.init();

//display all states
exports.state = async function () {
  let sql = 'SELECT code,state as state FROM state';
  let data = await db.query(sql);
  return data;

}

//display county related to the state selected
exports.county = async function (state) {
  const sql = "SELECT DISTINCT IFNULL(county,'Unknown') AS county from hosp_provider_info WHERE state='" + state + "' UNION SELECT DISTINCT IFNULL(county,'Unknown') AS county from snf_provider_info WHERE state='" + state + "' UNION SELECT DISTINCT IFNULL(county,'Unknown') AS county from hha_provider_info WHERE state='" + state + "' UNION SELECT DISTINCT IFNULL(county,'Unknown') AS county from renal_provider_info WHERE state='" + state + "' UNION SELECT DISTINCT IFNULL(county,'Unknown') AS county from hospice_provider_info WHERE state='" + state + "' ORDER BY county ASC";
  const data = await db.query(sql);
  return data;
}

//display city related to the selected state and county
exports.city = async function (state, county) {
  const sql = "SELECT DISTINCT city from hosp_provider_info where state='" + state + "' and county='" + county + "' UNION SELECT DISTINCT city from snf_provider_info where state='" + state + "' and county='" + county + "' UNION SELECT DISTINCT city from hospice_provider_info where state='" + state + "' and county='" + county + "' UNION SELECT DISTINCT city from renal_provider_info where state='" + state + "' and county='" + county + "' UNION SELECT DISTINCT city from hha_provider_info where state='" + state + "' and county='" + county + "' order by city ASC"
  const data = await db.query(sql);
  return data;
}

//hospitals related to the selected state, county and city
exports.hospitals = async function (state, county, city) {
  const sql = "SELECT prvdr_num  as pId, name, address, zip, phone, state, city, provider_type as type FROM hosp_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' UNION SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'SNF' as type FROM snf_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' UNION SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'HHA' as type FROM hha_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' UNION SELECT prvdr_num  as pId, name, address, zip, phone, state, City, 'HOSPICE' as type FROM hospice_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' UNION SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'RENAL' as type FROM renal_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' order by name ASC";
  const data = await db.query(sql);
  return data;
}

//hospitals available only in hha table for plan HHA with selected state, county and city
exports.hhahospitals = async function (state, county, city) {
  const sql = "SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'HHA' as type FROM hha_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' order by name ASC";
  const data = await db.query(sql);
  return data;
}


//hospitals available only in hospital table for plan HOSPITAL with selected state, county and city
exports.hospitalhospitals = async function (state, county, city) {
  const sql = "SELECT prvdr_num  as pId, name, address, zip, phone, state, city, provider_type as type FROM hosp_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' order by name ASC";
  const data = await db.query(sql);
  return data;
}

//hospitals available only in snf table for plan SNF with selected state, county and city
exports.snfhospitals = async function (state, county, city) {
  const sql = "SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'SNF' as type FROM snf_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' order by name ASC";
  const data = await db.query(sql);
  return data;
}

//hospitals available only in renal table for plan RENAL with selected state, county and city
exports.renalhospitals = async function (state, county, city) {
  const sql = "SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'RENAL' as type FROM renal_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' order by name ASC";
  const data = await db.query(sql);
  return data;
}

//hospitals available only in hospice for plan HOSPICE with selected state, county and city
exports.hospicehospitals = async function (state, county, city) {
  const sql = "SELECT prvdr_num  as pId, name, address, zip, phone, state, city, 'HOSPICE' as type FROM hospice_provider_info where state='" + state + "' and county='" + county + "' and city='" + city + "' order by name ASC";
  const data = await db.query(sql);
  return data;
}

//fetch all the available financial year for the selected hospital
exports.Financial_Year = async function (state, county, city, type, hospitals) {
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  const sql = "SELECT address, zip, phone FROM " + type.toLowerCase() + "_provider_info where prvdr_num ='" + pId + "'";
  const address = await db.query(sql);
  const sql1 = "SELECT DISTINCT(FY_END_DT), year(FY_END_DT) as Financial_Year, DATE_FORMAT(FY_BGN_DT,'%Y-%m-%d') as Begin_Date, DATE_FORMAT(FY_END_DT,'%Y-%m-%d') as End_Date, DATE_FORMAT(FI_RCPT_DT,'%Y-%m-%d') as Report_Date, REPORT_STATUS_CD as Status from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = '" + pId + "' ORDER BY year(FY_END_DT) DESC, DATE_FORMAT(FI_RCPT_DT,'%Y-%m-%d') DESC";
  const result = await db.query(sql1);
  return { address: address, data: result };
}

//get only the year of financial end date to display as table header in financial statement & balance sheet
exports.FYear = async function (state, county, city, type, hospitals) {
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  const sql1 = "SELECT DISTINCT(FY_END_DT), year(FY_END_DT) as Financial_Year from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = '" + pId + "' ORDER BY year(FY_END_DT) ASC";
  const FYear = await db.query(sql1);
  if (FYear == '') {
    return 0;
  }
  return FYear;
}

exports.FYearwithPlan = async function (state, county, city, type, hospitals) {
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  const sql1 = "SELECT DATE_FORMAT(FY_END_DT, '%Y-%m-%d') as Financial_Year from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = '" + pId + "' ORDER BY Financial_Year ASC";
  const FYear = await db.query(sql1);
  if (FYear == '') {
    return 0;
  }
  return FYear;
}

//get only the year of financial end date to display as table header in financial statement & balance sheet for year page displayed for search by facility type
exports.searchFYear = async function (pId, type) {
  const sql1 = "SELECT year(FY_END_DT) as Financial_Year from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = '" + pId + "' ORDER BY year(FY_END_DT) ASC";
  const searchFYear = await db.query(sql1);
  return searchFYear;
}

exports.searchFYearPlan = async function (pId, type) {
  const sql1 = "SELECT DATE_FORMAT(FY_END_DT, '%Y-%m-%d')  as Financial_Year from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = '" + pId + "' ORDER BY FY_END_DT ASC";
  const searchFYear = await db.query(sql1);
  return searchFYear;
}


//page displayed after search by facility type
exports.search = async function (pId, type) {
  if(type == "HOSP"){
    var typeName = "HOSP";
  }else if(type == "SNF"){
    var typeName = "SNF";
  }else if(type == "HOSPICE"){
    var typeName = "HOSPICE";
  }else if(type == "RENAL"){
    var typeName = "RENAL"
  }else{
    var typeName ="HHA";
  }
  const sql1 = "SELECT year(r.FY_END_DT) as Financial_Year, DATE_FORMAT(r.FY_BGN_DT, '%Y-%m-%d') as Begin_Date, DATE_FORMAT(r.FY_END_DT, '%Y-%m-%d') as End_Date, DATE_FORMAT(r.FI_RCPT_DT, '%Y-%m-%d') as Report_Date, '"+ typeName +"' as type, p.address, p.city, p.county, p.state, p.name, p.zip, p.phone, r.REPORT_STATUS_CD as Status from " + type.toLowerCase() + "_report_table r, " + type.toLowerCase() + "_provider_info p WHERE p.prvdr_num  = '"+ pId +" ' and p.prvdr_num  = r.PRVDR_NUM ORDER BY Financial_Year DESC "
  
  //const sql1 = "SELECT year(r.FY_END_DT) as Financial_Year, DATE_FORMAT(r.FY_BGN_DT, '%Y-%m-%d') as Begin_Date, DATE_FORMAT(r.FY_END_DT, '%Y-%m-%d') as End_Date, DATE_FORMAT(r.FI_RCPT_DT, '%Y-%m-%d') as Report_Date, p.provider_type as type, p.address, p.city, p.county, p.state, p.name, p.zip, p.phone, r.REPORT_STATUS_CD as Status from hosp_report_table r, hosp_provider_info p WHERE p.prvdr_num  = '" + pId + "' and p.prvdr_num  = r.PRVDR_NUM UNION SELECT year(r.FY_END_DT) as Financial_Year, DATE_FORMAT(r.FY_BGN_DT, '%Y-%m-%d') as Begin_Date, DATE_FORMAT(r.FY_END_DT, '%Y-%m-%d') as End_Date, DATE_FORMAT(r.FI_RCPT_DT, '%Y-%m-%d') as Report_Date, 'SNF' as type, p.address, p.city, p.county, p.state, p.name, p.zip, p.phone, r.REPORT_STATUS_CD as Status from snf_report_table r, snf_provider_info p WHERE p.prvdr_num  = '" + pId + "' and p.prvdr_num  = r.PRVDR_NUM UNION SELECT year(r.FY_END_DT) as Financial_Year, DATE_FORMAT(r.FY_BGN_DT, '%Y-%m-%d') as Begin_Date, DATE_FORMAT(r.FY_END_DT, '%Y-%m-%d') as End_Date, DATE_FORMAT(r.FI_RCPT_DT, '%Y-%m-%d') as Report_Date, 'HHA' as type, p.address, p.city, p.county, p.state, p.name, p.zip, p.phone, r.REPORT_STATUS_CD as Status from hha_report_table r, hha_provider_info p WHERE p.prvdr_num  = '" + pId + "' and p.prvdr_num  = r.PRVDR_NUM UNION SELECT year(r.FY_END_DT) as Financial_Year, DATE_FORMAT(r.FY_BGN_DT, '%Y-%m-%d') as Begin_Date, DATE_FORMAT(r.FY_END_DT, '%Y-%m-%d') as End_Date, DATE_FORMAT(r.FI_RCPT_DT, '%Y-%m-%d') as Report_Date, 'HOSPICE' as type, p.address, p.city, p.county, p.state, p.name, p.zip, p.phone, r.REPORT_STATUS_CD as Status from hospice_report_table r, hospice_provider_info p WHERE p.prvdr_num  = '" + pId + "' and p.prvdr_num  = r.PRVDR_NUM UNION SELECT year(r.FY_END_DT) as Financial_Year, DATE_FORMAT(r.FY_BGN_DT, '%Y-%m-%d') as Begin_Date, DATE_FORMAT(r.FY_END_DT, '%Y-%m-%d') as End_Date, DATE_FORMAT(r.FI_RCPT_DT, '%Y-%m-%d') as Report_Date, 'RENAL' as type, p.address, p.city, p.county, p.state, p.name, p.zip, p.phone, r.REPORT_STATUS_CD as Status from renal_report_table r, renal_provider_info p WHERE p.prvdr_num  = '" + pId + "' and p.prvdr_num  = r.PRVDR_NUM ORDER BY Financial_Year DESC";
  var result = await db.query(sql1);
  var sql = "SELECT name, address, city, county, state, zip, phone from hosp_provider_info where prvdr_num  = '" + pId + "'  UNION SELECT name, address, city, county, state, zip, phone from snf_provider_info where prvdr_num  = '" + pId + "' UNION SELECT name, address, city, county, state, zip, phone from hha_provider_info where prvdr_num  = '" + pId + "' UNION SELECT name, address, city, county, state, zip, phone from hospice_provider_info where prvdr_num  = '" + pId + "' UNION SELECT name, address, city, county, state, zip, phone from renal_provider_info where prvdr_num  = '" + pId + "'";
  var results = await db.query(sql);
  return { result: result, results: results};
}

//find type of that particular hospital in search by facility type
exports.findType = async function (pId) {
  const sql = "SELECT provider_type as type from hosp_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'SNF' as type from snf_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'HOSPICE' as type from hospice_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'RENAL' as type from renal_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'HHA' as type from hha_provider_info where prvdr_num = '" + pId + "'";
  var type = await db.query(sql);
  return JSON.parse(JSON.stringify(type[0].type));
}

//find type using url
exports.Type = async function (url) {
  var Url = url.replace(/%20/g, " ");
  decodeURI(Url);
  var hospital = Url.split('/')[7];
  var hsptl = hospital.split('-');
  var pId = hsptl[0];
  var name = hsptl[1];
  const sql = "SELECT provider_type as type from hosp_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'SNF' as type from snf_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'HOSPICE' as type from hospice_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'RENAL' as type from renal_provider_info where prvdr_num = '" + pId + "' UNION SELECT 'HHA' as type from hha_provider_info where prvdr_num = '" + pId + "'";
  var type = await db.query(sql);
  return JSON.parse(JSON.stringify(type[0].type));
}



/*
* masterdata.reports()
*/
exports.reports = async function (state, county, city, type, hospitals, Financial_Year) {
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  const sql = "SELECT address, zip, phone FROM " + type.toLowerCase() + "_provider_info where prvdr_num ='" + pId + "'";
  const data = await db.query(sql);
  const sql1 = "SELECT RPT_REC_NUM FROM " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = '" + pId + "' and year(FY_END_DT) = '" + Financial_Year + "'";
  const results = await db.query(sql1);
  var recordId = '';
  if (results.length > 0) {
    recordId = results[0].RPT_REC_NUM;
  }
  data['recordId'] = recordId;

  var sql2 = "SELECT DISTINCT n.WKSHT_CD from " + type.toLowerCase() + "_numeric_table n, " + type.toLowerCase() + "_report_table r where r.PRVDR_NUM='" + pId + "' and year(r.FY_END_DT) ='" + Financial_Year + "' and n.RPT_REC_NUM=r.RPT_REC_NUM ORDER by n.WKSHT_CD ASC";
  var result = await db.query(sql2);
  return { data: data, result: result, results: results };
}

//get only the last financial year of every hospital
exports.getLastYear = async function (hospitals, type) {
  const hsptl = hospitals.split('-');
  const pId = hsptl[0];
  const name = hsptl[1];
  const sql1 = "SELECT year(FY_END_DT) as year  from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = " + pId + " LIMIT 1";
  const result = await db.query(sql1);
  if (result == '') {
    return 0;
  }
  return JSON.stringify(result[0].year);
}

//get only the last financial year of every hospital for search by facility type
exports.getLastYearSearch = async function (pId, type) {
  const sql1 = "SELECT year(FY_END_DT) as year  from " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM = " + pId + " LIMIT 1";
  const result = await db.query(sql1);
  return JSON.stringify(result[0].year);
}

/*
* masterdata.getRecordId()
*/
exports.getRecordId = async function (type, hospitals, Financial_Year) {
  var hsptl = hospitals.split('-');
  var pId = hsptl[0];
  var sql = "SELECT RPT_REC_NUM FROM " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM =" + pId + " and year(FY_END_DT) =" + Financial_Year + "";
  var results = await db.query(sql);
  var recordId = '';
  if (results.length > 0) {
    recordId = results[0].RPT_REC_NUM;
  }
  return recordId;
}

/*
* masterdata.sheetId()
*/
exports.sheetId = async function (type, recordId, sheetId) {
  var data = {};
  if (recordId != '') {
    var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordId + "'  AND WKSHT_CD= '" + sheetId + "' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordId + "' AND WKSHT_CD= '" + sheetId + "' ORDER BY LINE_NUM,CLMN_NUM";
    results = await db.query(sql);
    for (var idx = 0; idx < results.length; idx++) {
      let row = results[idx];
      data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
    }
  }
  return data;
}

/*
* masterdata.hospitalInfo()
*/
exports.hospitalInfo = async function (state, county, city, type, hospitals, Financial_Year, sheetId) {
  var hsptl = hospitals.split('-');
  var pId = hsptl[0];
  const sql = "SELECT address, zip, phone FROM " + type.toLowerCase() + "_provider_info where prvdr_num ='" + pId + "'";
  const hospitalInfo = await db.query(sql);
  return JSON.parse(JSON.stringify(hospitalInfo[0]));
}


/*
* masterdata.year()
*/
exports.year = async function (state, county, city, type, hospitals, Financial_Year, sheetId) {
  var hsptl = hospitals.split('-');
  var pId = hsptl[0];
  var name = hsptl[1];
  const sql = "SELECT DATE_FORMAT(FY_BGN_DT,'%Y-%m-%d') as start, DATE_FORMAT(FY_END_DT,'%Y-%m-%d') as end from " + type.toLowerCase() + "_report_table where year(FY_END_DT)='" + Financial_Year + "' and PRVDR_NUM = '" + pId + "'";
  const year = await db.query(sql);
  return JSON.parse(JSON.stringify(year[0]));
}

exports.providers = async function (q, page) {
  var sql = 'SELECT prvdr_num  as id, provider_type as type, CONCAT(name,\', \',city,\', \',state,\', \',zip,\' - \',prvdr_num,\' - \',provider_type) as label FROM hosp_provider_info WHERE name LIKE \'' + q + '%\' OR prvdr_num  LIKE \'' + q + '%\' UNION SELECT prvdr_num as id, "SNF" as type, CONCAT(name,\', \',city,\', \',state,\', \',zip,\' - \',prvdr_num,\' - \',"SNF") as text FROM snf_provider_info WHERE name LIKE \'' + q + '%\' OR prvdr_num  LIKE \'' + q + '%\' UNION SELECT prvdr_num as id, "HHA" as type, CONCAT(name,\', \',city,\', \',state,\', \',zip,\' - \',prvdr_num,\' - \',"HHA") as text FROM hha_provider_info WHERE name LIKE \'' + q + '%\' OR prvdr_num  LIKE \'' + q + '%\' UNION SELECT prvdr_num as id, "HOSPICE" as type, CONCAT(name,\', \',city,\', \',state,\', \',zip,\' - \',prvdr_num,\' - \',"HOSPICE") as text FROM hospice_provider_info WHERE name LIKE \'' + q + '%\' OR prvdr_num  LIKE \'' + q + '%\' UNION SELECT prvdr_num as id, "RENAL" as type, CONCAT(name,\', \',city,\', \',state,\', \',zip,\' - \',prvdr_num,\' - \', "RENAL") as text FROM renal_provider_info WHERE name LIKE \'' + q + '%\' OR prvdr_num  LIKE \'' + q + '%\' ORDER BY label ASC';
  var results = await db.query(sql);
  return results;
}

exports.plan = async function (email) {
  var sql = "select plan from account where email = '" + email + "'";
  var results = await db.query(sql);
  return results[0].plan;
}

exports.sheetRecordId = async function (type, hospitals, lastYear) {
  var hsptl = hospitals.split('-');
  var pId = hsptl[0];
  var sql = "SELECT  RPT_REC_NUM FROM " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM =" + pId + " and year(FY_END_DT) ='" + lastYear + "'";
  var results = await db.query(sql);
  var recordId = '';
  if (results.length > 0) {
    recordId = results[0].RPT_REC_NUM;
  }
  return recordId;
}

//get record id for user with an plan
exports.sheetRecordIdwithPlan = async function (type, hospitals, lastYear) {
  var hsptl = hospitals.split('-');
  var pId = hsptl[0];
  var recordId = [];
  for (let i = 0; i < lastYear.length; i++) {
    let childArray = [lastYear[i].Financial_Year];
    let len = childArray.length;
    for (let j = 0; j < len; j++) {
      var year = childArray;
      var sql = "SELECT RPT_REC_NUM FROM " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM =" + pId + " and FY_END_DT ='" + year + "'";
      var results = await db.query(sql);
      if (results.length > 0) {
        for (let k = 0; k < results.length;) {
          recordId.push(results[k].RPT_REC_NUM);
          k++;
        };
      }
    }
  }
  return recordId;
}

//find record id (search by facility), user without a plan
exports.sheetRecordIdSearch = async function (type, pId, lastYearSearch) {
  var sql = "SELECT RPT_REC_NUM FROM " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM ='" + pId + "' and year(FY_END_DT) ='" + lastYearSearch + "'";
  var results = await db.query(sql);
  var recordIdSearch = '';
  if (results.length > 0) {
    recordIdSearch = results[0].RPT_REC_NUM;
  }
  return recordIdSearch;
}

//find record id (search by facility), user with a plan
exports.sheetRecordIdSearchwithPlan = async function (type, pId, lastYearSearch) {
  var recordId = [];
  for (let i = 0; i < lastYearSearch.length; i++) {
    let childArray = [lastYearSearch[i].Financial_Year];
    let len = childArray.length;
    for (let j = 0; j < len; j++) {
      var year = childArray;
      var sql = "SELECT DISTINCT(RPT_REC_NUM) FROM " + type.toLowerCase() + "_report_table WHERE PRVDR_NUM =" + pId + " and DATE_FORMAT(FY_END_DT, '%Y-%m-%d') ='" + year + "'";
      var results = await db.query(sql);
      if (results.length > 0) {
        for (let k = 0; k < results.length;) {
          recordId.push(results[k].RPT_REC_NUM);
          k++;
        };
      }
    }
  }
  return recordId;
}

//balance sheet data for hospital profile page (signed in not purchased any plan)
exports.balanceSheetId = async function (type, recordId) {

  var data = {};
  if (type == "HOSP" || type == "SNF") {
    if (recordId != '') {
      var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordId + "'  AND WKSHT_CD= 'G000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordId + "' AND WKSHT_CD= 'G000000' ORDER BY LINE_NUM,CLMN_NUM";
      results = await db.query(sql);
      for (var idx = 0; idx < results.length; idx++) {
        let row = results[idx];
        data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
      }
    }
    return data;
  }
  else {
    if (recordId != '') {
      var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordId + "'  AND WKSHT_CD= 'F000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordId + "' AND WKSHT_CD= 'F000000' ORDER BY LINE_NUM,CLMN_NUM";
      results = await db.query(sql);
      for (var idx = 0; idx < results.length; idx++) {
        let row = results[idx];
        data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
      }
    }
    return data;
  }

}

//balance sheet data for hospital profile page (signed in, purchased a plan)
exports.balanceSheetIdforPlan = async function (type, recordId) {
  if (type == "HOSP" || type == "SNF") {
    if (recordId != '') {
      var sheet = [];
      for (var i = 0; i < recordId.length; i++) {
        var record = recordId[i];
        var data = {};
        var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + record + "'  AND WKSHT_CD= 'G000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + record + "' AND WKSHT_CD= 'G000000' ORDER BY LINE_NUM,CLMN_NUM";
        results = await db.query(sql);
        for (var idx = 0; idx < results.length; idx++) {
          let row = results[idx];
          data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
        }
        sheet.push(data);
      }
      return sheet;
    }
  }
  else {
    if (recordId != '') {
      var sheet = [];
      for (var i = 0; i < recordId.length; i++) {
        var record = recordId[i];
        var data = {};
        var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + record + "'  AND WKSHT_CD= 'F000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + record + "' AND WKSHT_CD= 'F000000' ORDER BY LINE_NUM,CLMN_NUM";
        results = await db.query(sql);
        for (var idx = 0; idx < results.length; idx++) {
          let row = results[idx];
          data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
        }
        sheet.push(data);
      }
      return sheet;
    }
  }
}

// //balance sheet data for hospital profile (search by facility), user without a plan
// exports.balanceSheetIdSearch = async function (type, recordIdSearch) {

//   var data = {};
//   if (type == "HOSP" || type == "SNF") {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'G000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'G000000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
//   else {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'G000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'G000000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
// }

// //balance sheet data for hospital profile (search by facility), user with plan
// exports.balanceSheetIdSearchwithPlan = async function (type, recordIdSearch) {

//   var data = {};
//   if (type == "HOSP" || type == "SNF") {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'G000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'G000000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
//   else {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'G000000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'G000000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
// }


//income statement data for hospital profile (signed in, not purchased any plan)
exports.incomeStatement = async function (type, recordId) {
  var data = {};
  if (type == "HOSP" || type == "SNF") {
    if (recordId != '') {
      var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordId + "'  AND WKSHT_CD= 'G300000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordId + "' AND WKSHT_CD= 'G300000' ORDER BY LINE_NUM,CLMN_NUM";
      results = await db.query(sql);
      for (var idx = 0; idx < results.length; idx++) {
        let row = results[idx];
        data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
      }
    }
    return data;
  }
  else if (type == "HOSPICE") {
    if (recordId != '') {
      var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordId + "'  AND WKSHT_CD= 'F200000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordId + "' AND WKSHT_CD= 'F200000' ORDER BY LINE_NUM,CLMN_NUM";
      results = await db.query(sql);
      for (var idx = 0; idx < results.length; idx++) {
        let row = results[idx];
        data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
      }
    }
    return data;
  }
  else if (type == "RENAL" || type == "HHA") {
    if (recordId != '') {
      var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordId + "'  AND WKSHT_CD= 'F100000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordId + "' AND WKSHT_CD= 'F100000' ORDER BY LINE_NUM,CLMN_NUM";
      results = await db.query(sql);
      for (var idx = 0; idx < results.length; idx++) {
        let row = results[idx];
        data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
      }
    }
    return data;
  }
}

//incomestatement for user with plan
exports.incomeStatementforPlan = async function (type, recordId) {
  if (type == "HOSP" || type == "SNF") {
    if (recordId != '') {
      var income = [];
      for (var i = 0; i < recordId.length; i++) {
        var record = recordId[i];
        var data = {};
        var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + record + "'  AND WKSHT_CD= 'G300000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + record + "' AND WKSHT_CD= 'G300000' ORDER BY LINE_NUM,CLMN_NUM";
        results = await db.query(sql);
        for (var idx = 0; idx < results.length; idx++) {
          let row = results[idx];
          data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
        }
        income.push(data);
      }
      return income;
    }
  }
  else if (type == "HOSPICE") {
    if (recordId != '') {
      var income = [];
      for (var i = 0; i < recordId.length; i++) {
        var record = recordId[i];
        var data = {};
        var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + record + "'  AND WKSHT_CD= 'F200000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + record + "' AND WKSHT_CD= 'F200000' ORDER BY LINE_NUM,CLMN_NUM";
        results = await db.query(sql);
        for (var idx = 0; idx < results.length; idx++) {
          let row = results[idx];
          data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
        }
        income.push(data);
      }
      return income;
    }
  }
  else if (type == "RENAL" || type == "HHA") {
    if (recordId != '') {
      var income = [];
      for (var i = 0; i < recordId.length; i++) {
        var record = recordId[i];
        var data = {};
        var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + record + "'  AND WKSHT_CD= 'F100000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + record + "' AND WKSHT_CD= 'F100000' ORDER BY LINE_NUM,CLMN_NUM";
        results = await db.query(sql);
        for (var idx = 0; idx < results.length; idx++) {
          let row = results[idx];
          data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
          
        }
        income.push(data);
      }
      return income;
    }
  }
}
// //income statement for user(search by facility)
// exports.incomeStatementSearch = async function (type, recordIdSearch) {

//   var data = {};
//   if (type == "HOSP" || type == "SNF") {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'G300000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'G300000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
//   else if (type == "HOSPICE") {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'F200000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'F200000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
//   else if (type == "RENAL" || type == "HHA") {
//     if (recordIdSearch != '') {
//       var sql = "SELECT * FROM " + type.toLowerCase() + "_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "'  AND WKSHT_CD= 'F100000' UNION ALL SELECT * FROM " + type.toLowerCase() + "_alpha_numeric_table WHERE RPT_REC_NUM = '" + recordIdSearch + "' AND WKSHT_CD= 'F100000' ORDER BY LINE_NUM,CLMN_NUM";
//       results = await db.query(sql);
//       for (var idx = 0; idx < results.length; idx++) {
//         let row = results[idx];
//         data[row.LINE_NUM + '-' + row.CLMN_NUM] = row.ITM_VAL_NUM.replace('\r', '');
//       }
//     }
//     return data;
//   }
// }

exports.favorite = async function (user_id) {
  var sql = "SELECT * FROM downloaded_reports where user_id=" + user_id ;
  var favorite = await db.query(sql);
  return favorite;
}

exports.FindFav = async function(url){
  var Url = url.replace(/%20/g, " ");
  decodeURI(Url);
  var pId = Url.split('/')[4];
  var sql = 'SELECT DISTINCT(url) from favorites where url = \''+url+'\' and prvdr_num = "'+ pId +'"'; 
  var fav = await db.query(sql);
  if(fav.length>0){
    return JSON.parse(JSON.stringify(fav[0].url))
  }else{
    return null;
  }
  
}

exports.Fav = async function (url, type) {
  var Url = url.replace(/%20/g, " ");
  decodeURI(Url);
  var hospital = Url.split('/')[7];
  var hsptl = hospital.split('-');
  var pId = hsptl[0];
  var sql2 = "SELECT city, state, zip from " + type.toLowerCase() + "_provider_info where prvdr_num = '"+ pId +"'";
  var info = await db.query(sql2);
  var address = JSON.parse(JSON.stringify(info[0]))
  var name = hsptl[1];
  var bit = 1;
  var data = {
    prvdr_num: pId,
    name: name,
    city: address.city,
    state: address.state,
    zip: address.zip,
    bit: bit,
    url: url,
    type: type,
  }
  var sql1 = 'SELECT count(url) as count from favorites where url = \''+url+'\'';
  var count = await db.query(sql1);
  if(count.length>0){
    var isExist =  JSON.parse(JSON.stringify(count[0].count))
  }
  if(isExist == 0){
    var sql = "INSERT INTO favorites SET ?";
    var fav = await db.query(sql, data);
    return fav;
  }
  else{
    var sql1 = 'DELETE FROM favorites WHERE url = \''+url+'\'';
    var fav = await db.query(sql1);
    return fav;
  }
  
}

exports.favReports = async function (url, type) {
  var Url = url.replace(/%20/g, " ");
  decodeURI(Url);
  var hospital = Url.split('/')[7];
  var hsptl = hospital.split('-');
  var pId = hsptl[0];
  var sql2 = "SELECT city, state, zip from " + type.toLowerCase() + "_provider_info where prvdr_num = '"+ pId +"'";
  var info = await db.query(sql2);
  var address = JSON.parse(JSON.stringify(info[0]))
  var name = hsptl[1];
  var bit = 1;
  var data = {
    prvdr_num: pId,
    name: name,
    city: address.city,
    state: address.state,
    zip: address.zip,
    bit: bit,
    url: url,
    type: type,
  }
  var sql1 = 'SELECT count(url) as count from favorites where url = \''+url+'\' and prvdr_num = "'+pId+'"';
  var count = await db.query(sql1);
  if(count.length>0){
    var isExist =  JSON.parse(JSON.stringify(count[0].count))
  }
  if(isExist == 0){
    var sql = "INSERT INTO favorites SET ?";
    var fav = await db.query(sql, data);
    return fav;
  }
  else{
    var sql1 = 'DELETE FROM favorites WHERE url = \''+url+'\'';
    var fav = await db.query(sql1);
    return fav;
  }
}

exports.saveDownload = async function (url, type, user_id) {
  var Url = url.replace(/%20/g, " ");
  decodeURI(Url);
  var hospital = Url.split('/')[7];
  var hsptl = hospital.split('-');
  var pId = hsptl[0];
  var sql2 = "SELECT city, state, zip from " + type.toLowerCase() + "_provider_info where prvdr_num = '"+ pId +"'";
  var info = await db.query(sql2);
  var address = JSON.parse(JSON.stringify(info[0]))
  var name = hsptl[1];
  var data = {
    user_id: user_id,
    url: url,
    type: type,
    name: name,
  }
  var sql1 = 'SELECT count(url) as count from downloaded_reports where url = \''+url+'\' and user_id = "'+user_id+'"';
  var count = await db.query(sql1);
  if(count.length>0){
    var isExist =  JSON.parse(JSON.stringify(count[0].count))
  }
  console.log(count)
  if(isExist == 0){
    var sql = "INSERT INTO downloaded_reports SET ?";
    var fav = await db.query(sql, data);
    return fav;
  }
  /*else{
    var sql1 = 'DELETE FROM downloaded_reports WHERE url = \''+url+'\'';
    var fav = await db.query(sql1);
    return fav;
  }*/

}

exports.deleteDownload = async function (id) {
  if(id){
    var sql1 = "DELETE FROM downloaded_reports WHERE id = "+id;
    var fav = await db.query(sql1);
    return fav;
  }
}



exports.FavSearchReport = async function (url, pId, type) {
  var sql = "SELECT name, city, state, zip from " + type.toLowerCase() + "_provider_info WHERE prvdr_num = " + pId + "";
  var res = await db.query(sql);
  var sql2 = "SELECT count(url) as count from favorites where url = '" + url + "'";
  var count = await db.query(sql2);
  if(count.length>0){
    var isExist =  JSON.parse(JSON.stringify(count[0].count))
  }
  if(isExist == 0){
    var Url = url.replace(/%20/g, " ");
    decodeURI(Url);
    var name = res[0].name;
    var city = res[0].city;
    var state = res[0].state;
    var zip = res[0].zip;
    var bit = 1;
    var data = {
      prvdr_num: pId,
      name: name,
      city:city,
      state: state,
      zip: zip,
      bit: bit,
      url: url,
      type: type
    }
    var sql1 = "INSERT INTO favorites SET ?";
    var fav = await db.query(sql1, data);
    return fav;       
  }else{
    var sql1 = "DELETE FROM favorites WHERE url = '" + url + "'";
    var fav = await db.query(sql1);
    return fav;
  }
  
}

