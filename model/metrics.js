const db = require("./database");
const utilities = require("./utilities");
const chart = require("./chart");

db.init();

/*
* metrics.accounts()
* return the total number of registered accounts
*/

exports.accounts = async function(){

  const sql = "SELECT COUNT(account.id) as totalAccounts FROM account, user " +
  "WHERE account.id = user.account_id AND permission = 'owner'";

  const data = await db.query(sql);
  return data[0].totalAccounts;

}

/*
* metrics.accounts.active()
* return the total number of active paying accounts
*/

exports.accounts.active = async function(){

  const sql = "SELECT count(account.id) as activeAccounts FROM account, user " +
  "WHERE active = 1 and account.id = user.account_id AND permission = 'owner'";

  const data = await db.query(sql); 
  return data[0].activeAccounts;

}

/*
* metrics.accounts.churned()
* return the total number of churned (deactivated) accounts
*/

exports.accounts.churned = async function(){

  const sql = "SELECT count(account.id) as churnedAccounts FROM account, user " +
  "WHERE active = 0 and account.id = user.account_id AND permission = 'owner'";

  const data = await db.query(sql);
  return data[0].churnedAccounts;

}

/*
* metrics.growth()
* create a chart of user signups each month
*/

exports.growth = async function(){

  let months = [], totals = [];

  const sql = "SELECT count(id) as total, " +
  "EXTRACT(month FROM date_created) as month FROM account " +
  "GROUP BY EXTRACT(month FROM date_created)";

  const data = await db.query(sql);

  // get the months and count the totals
  for (i = 0; i < data.length; i++){
    months[i] = utilities.convertToMonthName(data[i].month);
    totals[i] = data[i].total;
  }

  // create the chart & send to frontend
  const userChart = new chart.line(months, totals, "users");
  return userChart;

}
