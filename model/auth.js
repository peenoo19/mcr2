const jwt = require("jsonwebtoken");
const settings = require("../settings");

/*
* return a JSON web token for authenticating API calls
*/

exports.token = function(accountId, userId, permission){

  return jwt.sign({

    accountId: accountId,
    userId: userId,
    permission: permission

  }, settings.token.secret, {

    expiresIn: settings.token.duration

  });
}

/*
* verify the token
*/

exports.verify = function(permission){

  return async function(req, res, next){

    try {

      const token = req.headers['authorization'];
      if (!token) throw new Error();

       // decode the json token
      let bearer = token.split(" ");
      const decode = jwt.verify(bearer[1], settings.token.secret);

      // verify user
      if (decode.accountId && decode.userId && decode.permission){

        // now verify the permission
        const userPermissions = getPermission(req);

        if (userPermissions[permission]){

          req.account = decode.accountId;
          req.user = decode.userId;
          req.permission = decode.permission;
          next();

        }
        else
          throw new Error();

      }
      else
        throw new Error();

    }
    catch (err){

      res.status(401).send({ message: "You do not have permission to perform this action. Please try logging in again" });

    }
  }
}

/*
* restrict access for a route to a certain user permission
*/

exports.access = function(permission, ignoreSubscription, redirect){

  return function(req, res, next){

    if (!redirect) redirect = "/signin";
    const userPermissions = getPermission(req);
    //console.log(userPermissions);
    // check user has permission
    if (userPermissions && userPermissions[permission]){

      // check user has an active subscription
      if (!ignoreSubscription && !req.session.subscription){

        res.redirect("/account/billing/failed");

      }
      else {

        req.permission = permission;
        next();

      }
    }
    else {

      res.redirect(redirect);

    }
  }
}

/*
* log the user out
*/

exports.signout = function(session){

  session.destroy();
  session = null;
  return true;

}

/*
* return a permission object from the request
*/

function getPermission(req){

  if (req.session.permission == "master")
    return { master: true, owner: true, admin: true, user: true }

  if (req.session.permission == "owner")
    return { master: false, owner: true, admin: true, user: true }

  else if (req.session.permission == "admin")
    return { master: false, owner: false, admin: true, user: true }

  else if (req.session.permission == "user")
    return { master: false, owner: false, admin: false, user: true }

}

exports.permission = getPermission;
