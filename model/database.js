const settings = require("../settings");
const sql = require("mysql");
var pool = null;


exports.init = function(reinit){

	// if pool doesn't exist or if reinitialising - create the pool
	if (!pool || reinit)
		pool = sql.createPool(settings.database);

};

exports.query = function(mysqlQuery, params){

	return new Promise(function(resolve, reject){

		pool.query(mysqlQuery, params, function(err, res){

			if (err) reject(err);
			else resolve(res);

		});
	});
}
