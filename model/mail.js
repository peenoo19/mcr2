const settings = require("../settings");
const file = require('fs');
const mailgun = require('mailgun-js');
const mail = new mailgun({ apiKey: settings.mailgun.apiKey, domain: settings.mailgun.domain });

/*
* mail.send()
* send an email using mailgun
* createEmail is called to construct the html with custom injected values
* template: name of HTML file in /emails
* content: object containing values to be injected, eg. { name: "Darth Vader" }
*/

exports.send = async function(to, subject, template, content){

	let email = {
		from: settings.mailgun.sender,
		to: to,
		subject: subject
	}

	// create the html email template
	email.html = await createEmail(template, content);

	//send the email
	mail.messages().send(email, function (err, res){

		if (err) throw(err);
		else return true;

	});
}

/*
* createEmail()
* opens an html email template and injects content into the {}
* templateName: name of the html file located in /emails without the file extension
* content: object containing the values to be injected
*/

function createEmail(templateName, content){

	return new Promise(function(resolve, reject){

		// get the template
		file.readFile("emails/" + templateName + ".html", "utf8", function(err, email) {

			if (err)
				reject(err);

			// inject content into {{braces}}
			for (key in content)
				email = email.replace("{{content." + key + "}}", content[key]);

			resolve(email);

		});
	});
}
