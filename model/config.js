const fs = require("fs");
const db = require("./database");
const settingsFile = "../settings.json";
let settings = require(settingsFile);

/*
* update the configuration settings
*/

exports.settings = async function(key, value){

  return new Promise((resolve, reject) => {

    settings[key] = value;

    fs.writeFile("settings.json", JSON.stringify(settings, null, " "), function(err){

      if (err)
        throw (err);
      else
        resolve({ success: true, message: key.charAt(0).toUpperCase()
          + key.slice(1) + " settings updated" });

    });
  });
}

/*
* create the database tables
*/

exports.database = function(){

  return new Promise(function(resolve, reject){

    db.init(true);

    // import the sql setup script file
    fs.readFile("setupDB.sql", "utf-8", async function(err, setupScript){

      try {

        const msg = await db.query(setupScript);
        resolve(true);

      }
      catch (err){

        reject(err);

      }
    });
  });
};

/*
* add the stripe key
*/

exports.stripeKey = function(publishableAPIKey){

  return new Promise(function(resolve, reject){

    fs.readFile("assets/js/account.js", "utf8", function(err, jsFile){

      if (err) reject(err);

      jsFile = jsFile.replace("YOUR_PUBLISHABLE_STRIPE_KEY", publishableAPIKey);

      fs.writeFile("assets/js/account.js", jsFile, function(err){

        if (err) reject(err)
        resolve(true);

      });
    });
  });
}

/*
* list the accounts
*/

exports.accounts = async function(){

  const sql = "SELECT account.id, account.email, plan, active, " +
  "date_format(account.date_created, '%d %b %Y %H:%i') as date_created, " +
  "user.name FROM account, user WHERE permission = 'owner' " +
  "AND user.account_id = account.id";

  return await db.query(sql);

}

/*
* list the users
*/

exports.users = async function(){

  const sql = "SELECT id, name, email, permission, account_id, " +
  "date_format(date_created, '%d %b %Y %H:%i') as date_created, " +
  "date_format(last_login, '%d %b %Y %H:%i') as last_login " +
  "FROM user WHERE permission != 'master'";

  return await db.query(sql);

}
