const settings = require("../settings");
const db = require("./database");
const account = require("./account");
const user = require("./user");
const metrics = require("./metrics");

db.init();

/*
* generate the view for each page
*/

exports.account = async function(accountId){

  try {
    const accountData = await account.get(accountId);

    return({
      title: "Your Account",
      subTitle: "Profile",
      email: accountData.email,
      userName: accountData.name,
      plan: accountData.plan
    });
  }
  catch(err){

    throw(err);

  }
}

exports.accountIdsOfUser = async function(userId) {
  const userData = await user.getUserByID(userId);
  return userData;
}

exports.getAllAccountIdsData = async function(accountIds) {
  const accountDatas = await account.getAccountByIds(accountIds);
  return accountDatas;
}

exports.account.users = async function(accountId, userId){

  try {

    const userInfo = await user.get(userId, null, accountId);
    const users = await account.users(accountId);

    return({

      title: "Your Account",
      subTitle: "Users",
      userName: userInfo.name,
      users: users,
      permissions: settings.permissions

    });
  }
  catch(err){

    throw(err);

  }
}

exports.account.billing = async function(accountId){

  try {

    let accountData = await account.get(accountId);

    return({

      title: "Your Account",
      subTitle: "Billing",
      plans: settings.stripe.plans,
      billingInterval: settings.stripe.billingInterval,
      currency: settings.stripe.currencySymbol,
      plan: accountData.plan,
      userName: accountData.name

    });
  }
  catch(err){

    throw(err);

  }
}

exports.user = async function(userId, accountId){

  try {

    const userInfo = await user.get(userId, null, accountId);

    return({

      title: "Your Account",
      subTitle: "Profile",
      email: userInfo.email,
      userName: userInfo.name

    });
  }
  catch (err){

    throw(err);

  }
}

exports.user.password = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Your Account",
      subTitle: "Password",
      userName: userData.name

    });
  }
  catch(err){

    throw(err);

  }
}

exports.dashboard = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);
    //console.log(userData)
    let accountData = await account.getUserPlans( userData.email);
    return({

      title: "Dashboard",
      userName: userData.name,
      planPurchased: accountData,

    });
  }
  catch(err){

    throw(err);

  }
}
exports.home = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Home",
      userName: userData.name,

    });
  }
  catch(err){

    throw(err);

  }
}
exports.state = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "All States",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.county = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Counties",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.city = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);
    
    return({

      title: "Cities",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.hospitals = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);
    return({

      title: "Hospitals",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.Financial_Year = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Financial Year",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.reports = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Reports",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.search = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Financial Year",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

exports.sheetId = async function(userId, accountId){

  try {

    const userData = await user.get(userId, null, accountId);

    return({

      title: "Reports",
      userName: userData.name,
      
    });
  }
  catch(err){

    throw(err);

  }
}

/*
* config views
*/

exports.config = async function() {

  // get the metrics
  const totalAccounts = await metrics.accounts();
  const activeAccounts = await metrics.accounts.active();
  const churnedAccounts = await metrics.accounts.churned();

  return {

    title: "Dashboard",
    totalAccounts: totalAccounts,
    activeAccounts: activeAccounts,
    churnedAccounts: churnedAccounts

  }
};
