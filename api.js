const express = require('express');
const settings = require("./settings");
const auth = require("./model/auth");
const masterdata = require("./model/masterdata");
const authController = require("./controller/authController");
const userController = require("./controller/userController");
const accountController = require("./controller/accountController");
const configController = require("./controller/configController");
const metricsController = require("./controller/metricsController");
const api = express.Router();

/*
* caller function for global error handling
* route all calls through this to try and handle errors
*/

const use = fn => (req, res, next) =>
  Promise.resolve(fn(req, res, next)).catch(next);

// account
api.post("/api/account", use(accountController.create));

api.post("/api/createaccount", use(accountController.createAccount));

api.put("/api/account", auth.verify("owner"), use(accountController.update));

api.put("/api/account/plan", auth.verify("owner"), use(accountController.update.plan));

api.put("/api/account/card", auth.verify("owner"), use(accountController.update.card));

api.delete("/api/account", auth.verify("owner"), use(accountController.close));

// user
api.post("/api/user/auth", use(authController.signin));

api.post("/api/user/invite", auth.verify("admin"), use(userController.invite));

api.post("/api/user", use(userController.create));

api.put("/api/user", auth.verify("user"), use(userController.update));

api.put("/api/user/password", auth.verify("user"), use(userController.update.password));

api.post("/api/user/resetpassword", use(userController.update.password.forgotten));

api.delete("/api/user", auth.verify("admin"), use(userController.delete));




// config user accounts
api.put("/api/config/account", auth.verify("master"), use(configController.account.update));

api.delete("/api/config/account", auth.verify("master"), use(configController.account.close));

api.put("/api/config/user", auth.verify("master"), use(configController.user));

api.delete("/api/config/user", auth.verify("master"), use(configController.user.delete));

// metrics
api.get("/api/metrics/accounts/growth", auth.verify("master"), use(metricsController.growth));

api.get("/api/metrics/accounts/total", auth.verify("master"), use(metricsController.accounts));

api.get("/api/metrics/accounts/active/total", auth.verify("master"), use(metricsController.accounts.active));

api.get("/api/metrics/accounts/churned/total", auth.verify("master"), use(metricsController.accounts.churned));

// setup
api.post("/api/config/database", use(configController.database));

api.post("/api/config/account", use(configController.account));

api.post("/api/config/stripe", use(configController.stripe));

api.post("/api/config/mailgun", use(configController.mailgun));



module.exports = api;
